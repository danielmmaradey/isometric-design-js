function load() {
	document.getElementById("content-svg").innerHTML = svg;
}
window.onload = load;

var svg = `
		
<svg id="svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="3871.322px" height="1047.577px" viewBox="0 0 3871.322 1047.577" enable-background="new 0 0 3871.322 1047.577"
	 xml:space="preserve">
<g id="cloud4">
	<path fill="#F7F8F9" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M3735.439,609.595
		c8.627-38.824-25.629-34.51-25.629-34.51c-72.004-99.217-138.293,0-138.293,0s-42.057-1.079-23.725,34.51H3735.439z"/>
</g>
<g id="build6_1_">
	<g>
		
			<rect x="3353.673" y="808.863" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="356.137" height="136.124"/>
		<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="3622.996,919.643 3622.996,944.987 
			3622.996,1003.946 3644.027,1003.946 3644.027,944.987 3871.576,944.987 3871.576,919.643 		"/>
		
			<rect x="3442.105" y="698.58" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="180.891" height="111.006"/>
		
			<rect x="3442.105" y="708.81" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="180.891" height="9.142"/>
		
			<rect x="3488.15" y="688.408" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="111.406" height="10.172"/>
		
			<rect x="3709.81" y="733.723" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="161.766" height="185.92"/>
		
			<rect x="3807.408" y="575.085" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="64.168" height="158.638"/>
		
			<rect x="3747.287" y="733.723" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="43.406" height="185.92"/>
		
			<rect x="3832.212" y="736.087" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="39.363" height="183.556"/>
		
			<rect x="3709.81" y="773.351" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="161.766" height="35.513"/>
		
			<rect x="3709.81" y="844.043" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="161.512" height="42.379"/>
		
			<rect x="3807.408" y="609.595" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="64.168" height="11.896"/>
		
			<rect x="3807.408" y="644.104" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="63.914" height="12.897"/>
		
			<rect x="3807.408" y="678.614" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="63.914" height="20.435"/>
		
			<rect x="3832.212" y="609.595" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="19.68" height="11.298"/>
		
			<rect x="3842.052" y="644.104" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0" height="13.964"/>
		
			<rect x="3851.892" y="678.614" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0" height="18.278"/>
		
			<rect x="3376.037" y="830.617" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0" height="61.323"/>
		
			<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3687.97" y1="830.617" x2="3687.97" y2="891.94"/>
		
			<rect x="3599.556" y="914.483" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0" height="30.504"/>
		
			<rect x="3442.105" y="914.483" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0" height="30.504"/>
		
			<rect x="3402.572" y="825.795" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="20.123" height="22.563"/>
		
			<rect x="3402.572" y="863.852" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="20.123" height="22.562"/>
		
			<rect x="3444.988" y="824.359" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="20.123" height="22.562"/>
		
			<rect x="3444.988" y="862.415" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="20.123" height="22.563"/>
		
			<rect x="3514.007" y="828.672" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="20.123" height="22.562"/>
		
			<rect x="3514.007" y="866.729" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="20.123" height="22.563"/>
		
			<rect x="3595.968" y="825.134" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="20.123" height="22.563"/>
		
			<rect x="3595.968" y="863.19" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="20.123" height="22.562"/>
		
			<rect x="3644.027" y="826.729" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="20.123" height="22.563"/>
		
			<rect x="3644.027" y="864.786" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="20.123" height="22.564"/>
		
			<rect x="3463.675" y="736.087" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.725" height="64.239"/>
		
			<rect x="3502.498" y="735.928" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.727" height="64.236"/>
		
			<rect x="3543.15" y="736.166" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.727" height="64.238"/>
		
			<rect x="3581.974" y="736.006" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.725" height="64.239"/>
		
			<rect x="3566.876" y="662.383" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0" height="25.373"/>
	</g>
</g>
<g id="cloud5">
	<path fill="#F7F8F9" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M894.416,435.284l-347.747,0.589
		c0,0-10.633-30.507,17.87-23.834c0,0,6.934-33.229,41.443-14.896c0,0,19.412-10.094,28.039,4.659c0,0,6.981-45.64,54.177-22.992
		c0,0,51.927-111.795,129.905-6.307c0,0,52.332-23.985,69.586,13.76c0,0,30.613,7.55,20.16,36.667
		C907.85,422.93,902.213,435.271,894.416,435.284z"/>
</g>
<g id="build7_1_">
	
		<rect x="-0.544" y="596.99" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="358.246" height="27.748"/>
	
		<rect x="232.48" y="319.497" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.936" height="377.956"/>
	<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M-0.544,511.457
		c0,0,220.012,33.435,233.024-191.96"/>
	<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M496.609,509.502
		c0,0-222.29,33.439-235.438-191.959"/>
	<g>
		<g>
			
				<rect x="283.011" y="409.984" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0.153" height="187.006"/>
			
				<rect x="318.036" y="458.835" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0.239" height="138.539"/>
			
				<rect x="341.292" y="477.238" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0.015" height="119.752"/>
		</g>
	</g>
	<g>
		
			<rect x="2490.927" y="811.519" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="179.02" height="0"/>
		
			<rect x="2669.947" y="811.519" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0" height="133.469"/>
		
			<rect x="2562.357" y="944.987" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="107.59" height="0"/>
		
			<rect x="2650.068" y="837.412" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0" height="107.575"/>
		
			<rect x="2538.884" y="868.755" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="69.482" height="0"/>
		
			<rect x="2608.367" y="868.755" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0" height="76.232"/>
		
			<rect x="2515.08" y="837.412" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0" height="21.284"/>
	</g>
	<g>
		
			<rect x="155.989" y="477.238" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0.032" height="119.752"/>
		
			<rect x="125.729" y="494.203" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0.038" height="102.787"/>
		
			<rect x="95.094" y="504.837" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0.242" height="92.153"/>
		
			<rect x="61.667" y="511.254" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0.177" height="85.736"/>
		
			<rect x="192.229" y="442.281" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0.123" height="154.709"/>
		
			<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="27.729" y1="513.199" x2="27.729" y2="596.99"/>
	</g>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="2490.927" y1="811.519" x2="2669.947" y2="811.291"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="2515.08" y1="837.412" x2="2515.546" y2="858.696"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="2650.068" y1="837.412" x2="2650.068" y2="944.987"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="2538.371" y1="868.755" x2="2608.367" y2="868.755"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="2608.367" y1="944.987" x2="2608.367" y2="868.755"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1791.792" y1="655.293" x2="1856.863" y2="655.293"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3042.587" y1="698.58" x2="3098.919" y2="698.58"/>
</g>
<g id="cloud6">
	<path fill="#F7F8F9" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M165.702,480.415
		c2.372-14.868-15.427-8.196-15.427-8.196c-10.513-23.473-26.472-13.841-26.472-13.841c-33.357-44.571-55.478,0-55.478,0
		s-19.393-17.786-31.726,4.481c0,0-15.949,4.862-5.076,17.556H165.702z"/>
</g>
<g id="build3_1_">
	<g>
		
			<rect x="1791.792" y="655.293" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="65.07" height="0"/>
		
			<rect x="1684.908" y="449.055" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="77.252" height="11.757"/>
		
			<rect x="1684.908" y="460.812" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="94.254" height="6.757"/>
		<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="1684.511,467.569 1684.511,677.535 
			1684.511,843.951 1738.151,843.951 1738.151,677.535 1738.151,655.293 1791.792,655.293 1791.792,467.569 		"/>
		<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="1856.863,617.927 1856.863,654.17 
			1856.863,858.696 2181.681,858.696 2181.681,654.17 2181.681,617.927 		"/>
		
			<rect x="1701.513" y="490.226" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.137" height="14.907"/>
		
			<rect x="1746.935" y="490.226" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.134" height="14.907"/>
		
			<rect x="1700.873" y="524.832" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.137" height="14.907"/>
		
			<rect x="1746.294" y="524.832" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.135" height="14.907"/>
		
			<rect x="1701.194" y="557.041" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.135" height="14.909"/>
		
			<rect x="1746.615" y="557.041" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.135" height="14.909"/>
		
			<rect x="1700.554" y="591.647" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.134" height="14.909"/>
		
			<rect x="1745.974" y="591.647" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.135" height="14.909"/>
		
			<rect x="1700.57" y="624.738" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.136" height="14.909"/>
		
			<rect x="1745.991" y="624.738" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.135" height="14.909"/>
		
			<rect x="1701.035" y="656.4" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.135" height="14.909"/>
		
			<rect x="1700.394" y="691.007" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.135" height="14.909"/>
		
			<rect x="1700.713" y="723.218" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.135" height="14.907"/>
		
			<rect x="1700.074" y="757.824" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.135" height="14.907"/>
		
			<rect x="1700.089" y="790.913" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.136" height="14.909"/>
		
			<rect x="1738.151" y="677.535" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="118.712" height="40.037"/>
		
			<rect x="1739.511" y="749.048" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="117.352" height="32.591"/>
		
			<rect x="1738.151" y="805.822" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="118.712" height="22.693"/>
		
			<rect x="1791.792" y="677.535" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="32.537" height="40.07"/>
		
			<rect x="1773.069" y="749.048" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="34.991" height="32.591"/>
		
			<rect x="1809.611" y="805.822" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="38.822" height="22.693"/>
		
			<rect x="1856.863" y="717.572" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="324.818" height="13.102"/>
		
			<rect x="1881.81" y="717.572" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.846" height="13.102"/>
		
			<rect x="1932.458" y="717.572" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="21.914" height="13.102"/>
		
			<rect x="1982.464" y="717.572" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.383" height="13.102"/>
		
			<rect x="2056.15" y="717.572" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.031" height="13.102"/>
		
			<rect x="2108.117" y="717.572" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.92" height="13.102"/>
		
			<rect x="2149.494" y="717.572" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.846" height="13.102"/>
		
			<rect x="2021.64" y="717.572" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.031" height="13.102"/>
		
			<rect x="1856.863" y="764.269" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="324.818" height="13.102"/>
		
			<rect x="1880.222" y="764.269" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.845" height="13.102"/>
		
			<rect x="1930.871" y="764.269" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="21.914" height="13.102"/>
		
			<rect x="1980.875" y="764.269" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.385" height="13.102"/>
		
			<rect x="2054.56" y="764.269" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.033" height="13.102"/>
		
			<rect x="2106.529" y="764.269" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.918" height="13.102"/>
		
			<rect x="2147.906" y="764.269" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.844" height="13.102"/>
		
			<rect x="2020.05" y="764.269" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.033" height="13.102"/>
		
			<rect x="1856.863" y="811.519" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="324.104" height="13.101"/>
		
			<rect x="1880.222" y="811.519" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.845" height="13.101"/>
		
			<rect x="1930.871" y="811.519" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="21.914" height="13.101"/>
		
			<rect x="1980.875" y="811.519" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.385" height="13.101"/>
		
			<rect x="2054.56" y="811.519" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.033" height="13.101"/>
		
			<rect x="2106.529" y="811.519" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.918" height="13.101"/>
		
			<rect x="2147.906" y="811.519" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.844" height="13.101"/>
		
			<rect x="2020.05" y="811.519" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.033" height="13.101"/>
		
			<rect x="1856.863" y="670.983" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="324.818" height="13.102"/>
		
			<rect x="1880.222" y="670.983" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.845" height="13.102"/>
		
			<rect x="1930.871" y="670.983" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="21.914" height="13.102"/>
		
			<rect x="1980.875" y="670.983" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.385" height="13.102"/>
		
			<rect x="2054.56" y="670.983" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.033" height="13.102"/>
		
			<rect x="2106.529" y="670.983" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.918" height="13.102"/>
		
			<rect x="2147.906" y="670.983" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.844" height="13.102"/>
		
			<rect x="2020.05" y="670.983" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.033" height="13.102"/>
		
			<rect x="1856.863" y="633.096" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="324.664" height="13.102"/>
		
			<rect x="1882.371" y="633.096" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.845" height="13.102"/>
		
			<rect x="1933.019" y="633.096" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="21.914" height="13.102"/>
		
			<rect x="1983.023" y="633.096" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.385" height="13.102"/>
		
			<rect x="2056.71" y="633.096" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.031" height="13.102"/>
		
			<rect x="2108.677" y="633.096" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.92" height="13.102"/>
		
			<rect x="2150.054" y="633.096" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.846" height="13.102"/>
		
			<rect x="2022.201" y="633.096" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.031" height="13.102"/>
		
			<rect x="1738.151" y="717.572" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="118.712" height="31.476"/>
		
			<rect x="1739.511" y="781.639" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="117.352" height="24.184"/>
	</g>
	
		<rect x="2181.681" y="757.824" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="309.246" height="100.872"/>
	<g>
		<g>
			
				<rect x="2490.927" y="757.824" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="90.588" height="53.694"/>
			<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="2153.529,475.964 2153.529,617.927 
				2181.681,617.927 2181.681,756.506 2581.515,756.506 2581.515,475.964 			"/>
		</g>
		
			<rect x="2199.33" y="464.192" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="348.182" height="11.772"/>
		<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="2398.093,464.722 2516.792,464.625 
			2475.125,450.195 2373.15,450.276 2271.193,450.195 2229.525,464.625 2348.228,464.722 2348.337,464.763 2373.24,464.739 
			2397.984,464.763 		"/>
		
			<rect x="2353.226" y="475.964" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="40.643" height="281.86"/>
		
			<rect x="2181.681" y="698.465" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="171.545" height="59.359"/>
		
			<rect x="2393.869" y="698.465" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="187.646" height="59.359"/>
		
			<rect x="2181.681" y="617.927" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="171.545" height="38.474"/>
		
			<rect x="2393.869" y="616.232" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="187.646" height="40.168"/>
		
			<rect x="2153.529" y="475.964" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="199.697" height="48.868"/>
		
			<rect x="2393.869" y="475.964" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="187.646" height="48.868"/>
		
			<rect x="2153.529" y="557.041" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="199.697" height="14.909"/>
		
			<rect x="2393.869" y="558.166" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="187.646" height="13.784"/>
		
			<rect x="2430.115" y="477.284" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.303" height="280.54"/>
		
			<rect x="2502.107" y="477.284" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.877" height="280.54"/>
		
			<rect x="2292.496" y="477.284" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.883" height="280.54"/>
		
			<rect x="2199.33" y="475.964" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="31.352" height="281.86"/>
		
			<rect x="2367.523" y="477.284" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.877" height="280.54"/>
		
			<rect x="2205.699" y="835.171" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="2.139" height="23.525"/>
		
			<rect x="2452.162" y="837.412" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="2.822" height="21.284"/>
		
			<rect x="2203.525" y="781.639" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="21.688" height="17.49"/>
		
			<rect x="2254.345" y="782.168" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="21.688" height="17.492"/>
		
			<rect x="2302.74" y="781.639" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="21.688" height="17.49"/>
		
			<rect x="2353.56" y="782.168" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="21.689" height="17.492"/>
		
			<rect x="2398.933" y="781.373" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="21.686" height="17.49"/>
		
			<rect x="2449.751" y="781.903" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="21.688" height="17.49"/>
	</g>
	
		<rect x="2581.515" y="811.519" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="88.432" height="0"/>
	
		<rect x="2650.068" y="837.412" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0" height="107.575"/>
	
		<rect x="2562.357" y="944.987" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="1060.639" height="0"/>
	
		<rect x="2515.08" y="837.412" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0" height="21.284"/>
	
		<rect x="2538.884" y="868.755" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="69.482" height="0"/>
	
		<rect x="2608.367" y="868.755" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0" height="76.232"/>
	
		<rect x="2604.884" y="788.028" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="41.699" height="23.263"/>
	
		<rect x="2490.927" y="784.55" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="90.588" height="0"/>
	
		<rect x="2502.107" y="757.824" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.877" height="53.467"/>
</g>
<g id="cloud3">
	<path fill="#F7F8F9" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M3478.414,398.726
		c4.707-29.479-30.582-16.244-30.582-16.244c-20.844-46.541-52.49-27.449-52.49-27.449c-66.141-88.368-110,0-110,0
		s-38.453-35.259-62.904,8.889c0,0-31.623,9.638-10.063,34.804H3478.414z"/>
</g>
<g id="build4_1_">
	<g>
		
			<rect x="2669.947" y="423.362" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="56.078" height="521.625"/>
		
			<rect x="2726.025" y="404.691" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="119.689" height="540.296"/>
		
			<rect x="2845.714" y="423.362" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="53.926" height="521.625"/>
		
			<rect x="2899.64" y="730.674" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="125.098" height="214.313"/>
		<g>
			
				<rect x="2687.455" y="439.202" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2687.455" y="473.712" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2687.455" y="508.222" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2687.455" y="542.731" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2687.455" y="577.241" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2687.455" y="611.751" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2687.455" y="646.261" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2687.455" y="680.771" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2687.455" y="710.966" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2687.455" y="745.476" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.28"/>
			
				<rect x="2687.455" y="779.985" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.28"/>
			
				<rect x="2687.455" y="814.495" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.28"/>
			
				<rect x="2687.455" y="849.005" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.281"/>
			
				<rect x="2687.455" y="883.515" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.281"/>
			
				<rect x="2687.455" y="918.027" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
		</g>
		<g>
			
				<rect x="2860.939" y="437.044" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.28"/>
			
				<rect x="2860.939" y="471.554" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.28"/>
			
				<rect x="2860.939" y="506.063" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.28"/>
			
				<rect x="2860.939" y="540.573" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.28"/>
			
				<rect x="2860.939" y="575.085" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2860.939" y="609.595" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2860.939" y="644.104" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2860.939" y="678.614" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2860.939" y="708.81" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2860.939" y="743.319" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2860.939" y="777.829" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2860.939" y="812.339" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2860.939" y="846.849" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.278"/>
			
				<rect x="2860.939" y="881.359" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.277"/>
			
				<rect x="2860.939" y="915.869" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.473" height="18.277"/>
		</g>
		<g>
			<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="2742.277,404.647 2742.203,405.078 
				2756.662,405.078 2756.662,404.647 2756.626,328.414 			"/>
		</g>
		<g>
			<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="2829.96,404.647 2830.035,405.078 
				2815.578,405.078 2815.578,404.647 2815.611,328.412 			"/>
		</g>
		<g>
			
				<rect x="2740.804" y="432.731" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="36.438" height="24.749"/>
			
				<rect x="2792.568" y="432.731" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="36.439" height="24.749"/>
			
				<rect x="2740.804" y="467.241" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="36.438" height="24.749"/>
			
				<rect x="2792.568" y="467.241" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="36.439" height="24.749"/>
			
				<rect x="2740.804" y="501.751" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="36.438" height="24.749"/>
			
				<rect x="2792.568" y="501.751" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="36.439" height="24.749"/>
			
				<rect x="2740.804" y="536.261" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="36.438" height="24.749"/>
			
				<rect x="2792.568" y="536.261" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="36.439" height="24.749"/>
		</g>
		<g>
			
				<rect x="2736.49" y="584.227" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.076"/>
			
				<rect x="2771" y="584.227" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.076"/>
			
				<rect x="2805.509" y="584.227" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.076"/>
		</g>
		<g>
			
				<rect x="2736.49" y="621.49" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.078"/>
			
				<rect x="2771" y="621.49" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.078"/>
			
				<rect x="2805.509" y="621.49" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.078"/>
		</g>
		<g>
			
				<rect x="2736.49" y="657.002" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.079"/>
			
				<rect x="2771" y="657.002" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.079"/>
			
				<rect x="2805.509" y="657.002" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.079"/>
		</g>
		<g>
			
				<rect x="2736.49" y="694.269" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.078"/>
			
				<rect x="2771" y="694.269" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.078"/>
			
				<rect x="2805.509" y="694.269" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.078"/>
		</g>
		<g>
			
				<rect x="2736.49" y="727.548" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.079"/>
			
				<rect x="2771" y="727.548" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.079"/>
			
				<rect x="2805.509" y="727.548" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.079"/>
		</g>
		<g>
			
				<rect x="2736.49" y="764.813" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.076"/>
			
				<rect x="2771" y="764.813" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.076"/>
			
				<rect x="2805.509" y="764.813" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.076"/>
		</g>
		<g>
			
				<rect x="2736.49" y="800.326" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.076"/>
			
				<rect x="2771" y="800.326" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.076"/>
			
				<rect x="2805.509" y="800.326" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.076"/>
		</g>
		<g>
			
				<rect x="2736.49" y="837.59" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.078"/>
			
				<rect x="2771" y="837.59" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.078"/>
			
				<rect x="2805.509" y="837.59" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.078"/>
		</g>
		<g>
			
				<rect x="2736.49" y="869.346" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.076"/>
			
				<rect x="2771" y="869.346" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.076"/>
			
				<rect x="2805.509" y="869.346" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.076"/>
		</g>
		<g>
			
				<rect x="2736.49" y="906.609" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.078"/>
			
				<rect x="2771" y="906.609" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.078"/>
			
				<rect x="2805.509" y="906.609" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.846" height="17.078"/>
		</g>
		<g>
			<g>
				
					<rect x="2903.103" y="751.914" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.137" height="11.816"/>
				
					<rect x="2924.648" y="751.914" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.137" height="11.816"/>
				
					<rect x="2946.195" y="751.914" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.135" height="11.816"/>
			</g>
			<g>
				
					<rect x="2963.496" y="751.914" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.135" height="11.816"/>
				
					<rect x="2985.039" y="751.914" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.141" height="11.816"/>
				
					<rect x="3006.587" y="751.914" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.133" height="11.816"/>
			</g>
		</g>
		<g>
			<g>
				
					<rect x="2903.103" y="777.797" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.137" height="11.816"/>
				
					<rect x="2924.648" y="777.797" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.137" height="11.816"/>
				
					<rect x="2946.195" y="777.797" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.135" height="11.816"/>
			</g>
			<g>
				
					<rect x="2963.496" y="777.797" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.135" height="11.816"/>
				
					<rect x="2985.039" y="777.797" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.141" height="11.816"/>
				
					<rect x="3006.587" y="777.797" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.133" height="11.816"/>
			</g>
		</g>
		<g>
			<g>
				
					<rect x="2903.103" y="803.678" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.137" height="11.818"/>
				
					<rect x="2924.648" y="803.678" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.137" height="11.818"/>
				
					<rect x="2946.195" y="803.678" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.135" height="11.818"/>
			</g>
			<g>
				
					<rect x="2963.496" y="803.678" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.135" height="11.818"/>
				
					<rect x="2985.039" y="803.678" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.141" height="11.818"/>
				
					<rect x="3006.587" y="803.678" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.133" height="11.818"/>
			</g>
		</g>
		<g>
			<g>
				
					<rect x="2903.103" y="829.561" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.137" height="11.816"/>
				
					<rect x="2924.648" y="829.561" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.137" height="11.816"/>
				
					<rect x="2946.195" y="829.561" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.135" height="11.816"/>
			</g>
			<g>
				
					<rect x="2963.496" y="829.561" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.135" height="11.816"/>
				
					<rect x="2985.039" y="829.561" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.141" height="11.816"/>
				
					<rect x="3006.587" y="829.561" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.133" height="11.816"/>
			</g>
		</g>
		<g>
			<g>
				
					<rect x="2903.103" y="851.129" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.137" height="11.816"/>
				
					<rect x="2924.648" y="851.129" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.137" height="11.816"/>
				
					<rect x="2946.195" y="851.129" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.135" height="11.816"/>
			</g>
			<g>
				
					<rect x="2963.496" y="851.129" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.135" height="11.816"/>
				
					<rect x="2985.039" y="851.129" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.141" height="11.816"/>
				
					<rect x="3006.587" y="851.129" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.133" height="11.816"/>
			</g>
		</g>
		<g>
			<g>
				
					<rect x="2903.103" y="877.012" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.137" height="11.816"/>
				
					<rect x="2924.648" y="877.012" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.137" height="11.816"/>
				
					<rect x="2946.195" y="877.012" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.135" height="11.816"/>
			</g>
			<g>
				
					<rect x="2963.496" y="877.012" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.135" height="11.816"/>
				
					<rect x="2985.039" y="877.012" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.141" height="11.816"/>
				
					<rect x="3006.587" y="877.012" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.133" height="11.816"/>
			</g>
		</g>
		<g>
			<g>
				
					<rect x="2902.384" y="902.667" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.135" height="11.816"/>
				
					<rect x="2923.925" y="902.667" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.141" height="11.816"/>
				
					<rect x="2945.474" y="902.667" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.135" height="11.816"/>
			</g>
			<g>
				
					<rect x="2962.777" y="902.667" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.133" height="11.816"/>
				
					<rect x="2984.318" y="902.667" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.141" height="11.816"/>
				
					<rect x="3005.867" y="902.667" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="16.133" height="11.816"/>
			</g>
		</g>
		
			<rect x="2991.667" y="906.872" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="406.59" height="38.115"/>
	</g>
</g>
<g id="cloud1">
	<path fill="#F7F8F9" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2081.957,449.146
		c-11.502-47.413-54.797-28.085-54.797-28.085c-59.838-74.052-101.215,0-101.215,0l0.366,0.337
		c-0.152,0.194-0.325,0.375-0.468,0.572c-0.309-0.13-0.62-0.256-0.936-0.378l1.037-0.531
		c-35.231-13.657-42.418,21.568-42.418,21.568l0.834-0.42c-0.051,0.206-0.126,0.407-0.173,0.619l-0.661-0.199
		c0,0-23.009-9.756-23.009,28.558h50.327h69.764h1.533h131.418C2114.966,433.589,2081.957,449.146,2081.957,449.146z
		 M1977.71,469.907c0.279-0.018,0.539-0.042,0.809-0.068c0.227,0.026,0.455,0.035,0.674,0.059l0.707,0.642L1977.71,469.907z"/>
</g>
<g id="build1_1_">
	<g>
		<g id="build1_9_">
			<g>
				
					<rect x="631.96" y="893.559" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="713.923" height="118.629"/>
				
					<rect x="1848.433" y="893.559" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="713.924" height="118.629"/>
				<g>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M685.882,928.819
						c-1.809-9.101-9.572-15.933-18.875-15.933c-9.303,0-17.068,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H685.882z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M735.598,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.303,0-17.068,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H735.598z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M785.599,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.304,0-17.068,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H785.599z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M835.315,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.305,0-17.068,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H835.315z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M885.316,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.304,0-17.068,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H885.316z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M935.032,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.304,0-17.067,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H935.032z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M985.034,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.304,0-17.068,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H985.034z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1034.75,928.819
						c-1.81-9.101-9.574-15.933-18.875-15.933c-9.305,0-17.068,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H1034.75z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1084.751,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.303,0-17.067,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H1084.751z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1134.466,928.819
						c-1.81-9.101-9.573-15.933-18.874-15.933c-9.304,0-17.067,6.832-18.877,15.933h-3.731v24.053h45.689v-24.053H1134.466z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1184.468,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.303,0-17.067,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H1184.468z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1234.183,928.819
						c-1.81-9.101-9.573-15.933-18.875-15.933c-9.303,0-17.066,6.832-18.876,15.933h-3.73v24.053h45.687v-24.053H1234.183z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1283.334,928.819
						c-1.809-9.101-9.573-15.933-18.875-15.933c-9.303,0-17.066,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H1283.334z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1333.05,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.303,0-17.067,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H1333.05z"/>
				</g>
				<g>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1900.449,924.504
						c-1.809-9.097-9.574-15.932-18.875-15.932c-9.303,0-17.067,6.835-18.877,15.932h-3.73v24.056h45.689v-24.056H1900.449z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1950.169,924.504
						c-1.813-9.097-9.58-15.932-18.879-15.932c-9.307,0-17.066,6.835-18.877,15.932h-3.73v24.056h45.689v-24.056H1950.169z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2000.164,924.504
						c-1.803-9.097-9.57-15.932-18.871-15.932c-9.303,0-17.07,6.835-18.873,15.932h-3.732v24.056h45.691v-24.056H2000.164z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2049.88,924.504
						c-1.811-9.097-9.57-15.932-18.871-15.932s-17.07,6.835-18.883,15.932h-3.723v24.056h45.682v-24.056H2049.88z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2099.884,924.504
						c-1.811-9.097-9.57-15.932-18.871-15.932c-9.311,0-17.07,6.835-18.881,15.932h-3.732v24.056h45.689v-24.056H2099.884z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2149.603,924.504
						c-1.813-9.097-9.578-15.932-18.883-15.932c-9.301,0-17.061,6.835-18.871,15.932h-3.732v24.056h45.689v-24.056H2149.603z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2199.599,924.504
						c-1.805-9.097-9.57-15.932-18.873-15.932s-17.068,6.835-18.873,15.932h-3.732v24.056h45.689v-24.056H2199.599z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2249.316,924.504
						c-1.811-9.097-9.57-15.932-18.873-15.932c-9.301,0-17.068,6.835-18.881,15.932h-3.725v24.056h45.684v-24.056H2249.316z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2299.32,924.504
						c-1.811-9.097-9.57-15.932-18.873-15.932c-9.309,0-17.068,6.835-18.879,15.932h-3.732v24.056h45.689v-24.056H2299.32z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2349.039,924.504
						c-1.813-9.097-9.58-15.932-18.883-15.932c-9.301,0-17.061,6.835-18.871,15.932h-3.732v24.056h45.689v-24.056H2349.039z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2399.033,924.504
						c-1.803-9.097-9.57-15.932-18.871-15.932c-9.303,0-17.07,6.835-18.873,15.932h-3.732v24.056h45.688v-24.056H2399.033z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2448.75,924.504
						c-1.811-9.097-9.57-15.932-18.871-15.932s-17.07,6.835-18.883,15.932h-3.723v24.056h45.682v-24.056H2448.75z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2497.904,924.504
						c-1.813-9.097-9.572-15.932-18.873-15.932c-9.311,0-17.07,6.835-18.881,15.932h-3.732v24.056h45.689v-24.056H2497.904z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2547.621,924.504
						c-1.813-9.097-9.578-15.932-18.883-15.932c-9.301,0-17.059,6.835-18.871,15.932h-3.732v24.056h45.689v-24.056H2547.621z"/>
				</g>
				
					<rect x="1345.883" y="908.572" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="502.55" height="125.184"/>
				<g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M667.246,961.501
							c-14.294,0-25.883,22.698-25.883,50.687h51.765C693.127,984.199,681.539,961.501,667.246,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M716.958,961.501
							c-14.293,0-25.881,22.698-25.881,50.687h51.765C742.841,984.199,731.252,961.501,716.958,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M766.961,961.501
							c-14.294,0-25.883,22.698-25.883,50.687h51.765C792.843,984.199,781.254,961.501,766.961,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M816.821,961.501
							c-14.293,0-25.882,22.698-25.882,50.687h51.765C842.704,984.199,831.114,961.501,816.821,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M866.535,961.501
							c-14.294,0-25.883,22.698-25.883,50.687h51.765C892.417,984.199,880.828,961.501,866.535,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M916.536,961.501
							c-14.293,0-25.883,22.698-25.883,50.687h51.766C942.419,984.199,930.829,961.501,916.536,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M964.312,961.501
							c-14.293,0-25.882,22.698-25.882,50.687h51.765C990.194,984.199,978.605,961.501,964.312,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1014.025,961.501
							c-14.293,0-25.883,22.698-25.883,50.687h51.765C1039.907,984.199,1028.318,961.501,1014.025,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1064.026,961.501
							c-14.293,0-25.882,22.698-25.882,50.687h51.765C1089.909,984.199,1078.321,961.501,1064.026,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1113.888,961.501
							c-14.294,0-25.883,22.698-25.883,50.687h51.765C1139.77,984.199,1128.181,961.501,1113.888,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1163.601,961.501
							c-14.293,0-25.882,22.698-25.882,50.687h51.765C1189.483,984.199,1177.895,961.501,1163.601,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1213.603,961.501
							c-14.294,0-25.883,22.698-25.883,50.687h51.765C1239.485,984.199,1227.896,961.501,1213.603,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1262.541,961.501
							c-14.293,0-25.882,22.698-25.882,50.687h51.765C1288.423,984.199,1276.835,961.501,1262.541,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1312.542,961.501
							c-14.293,0-25.882,22.698-25.882,50.687h51.765C1338.425,984.199,1326.836,961.501,1312.542,961.501z"/>
					</g>
				</g>
				<g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1882.746,961.501
							c-14.293,0-25.883,22.698-25.883,50.687h51.766C1908.628,984.199,1897.04,961.501,1882.746,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1932.457,961.501
							c-14.289,0-25.881,22.698-25.881,50.687h51.766C1958.341,984.199,1946.757,961.501,1932.457,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1982.464,961.501
							c-14.299,0-25.883,22.698-25.883,50.687h51.762C2008.343,984.199,1996.751,961.501,1982.464,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2032.322,961.501
							c-14.295,0-25.883,22.698-25.883,50.687h51.768C2058.207,984.199,2046.613,961.501,2032.322,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2082.033,961.501
							c-14.289,0-25.883,22.698-25.883,50.687h51.764C2107.914,984.199,2096.33,961.501,2082.033,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2132.037,961.501
							c-14.289,0-25.883,22.698-25.883,50.687h51.766C2157.919,984.199,2146.333,961.501,2132.037,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2179.816,961.501
							c-14.299,0-25.883,22.698-25.883,50.687h51.766C2205.699,984.199,2194.107,961.501,2179.816,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2229.525,961.501
							c-14.289,0-25.881,22.698-25.881,50.687h51.764C2255.408,984.199,2243.824,961.501,2229.525,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2279.529,961.501
							c-14.297,0-25.881,22.698-25.881,50.687h51.764C2305.412,984.199,2293.818,961.501,2279.529,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2329.39,961.501
							c-14.299,0-25.883,22.698-25.883,50.687h51.766C2355.273,984.199,2343.679,961.501,2329.39,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2379.107,961.501
							c-14.297,0-25.881,22.698-25.881,50.687h51.764C2404.99,984.199,2393.396,961.501,2379.107,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2429.103,961.501
							c-14.289,0-25.883,22.698-25.883,50.687h51.764C2454.984,984.199,2443.4,961.501,2429.103,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2478.046,961.501
							c-14.299,0-25.885,22.698-25.885,50.687h51.768C2503.929,984.199,2492.335,961.501,2478.046,961.501z"/>
					</g>
					<g>
						<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2528.048,961.501
							c-14.299,0-25.883,22.698-25.883,50.687h51.766C2553.931,984.199,2542.339,961.501,2528.048,961.501z"/>
					</g>
				</g>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="631.96" y1="893.559" x2="657.843" y2="859.049"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1345.883" y1="893.559" x2="1380.394" y2="843.951"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1380.394" y1="843.951" x2="1809.611" y2="843.951"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1809.611" y1="843.951" x2="1848.433" y2="893.559"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="657.843" y1="859.049" x2="1369.891" y2="859.049"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1821.427" y1="859.049" x2="2528.984" y2="859.049"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="2528.984" y1="859.049" x2="2562.357" y2="893.559"/>
			</g>
			<g>
				<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="1460.198,908.572 1460.198,999.246 
					1473.139,999.246 1473.139,1033.756 1533.531,1033.756 1533.531,999.246 1546.472,999.246 1546.472,908.572 				"/>
				<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="1563.727,908.572 1563.727,999.246 
					1576.668,999.246 1576.668,1033.756 1637.06,1033.756 1637.06,999.246 1650.001,999.246 1650.001,908.572 				"/>
				<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="1662.943,908.572 1662.943,999.246 
					1675.884,999.246 1675.884,1033.756 1736.276,1033.756 1736.276,999.246 1749.218,999.246 1749.218,908.572 				"/>
				<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="1762.16,908.572 1762.16,999.246 
					1771.05,999.246 1771.05,1033.756 1812.537,1033.756 1812.537,999.246 1821.427,999.246 1821.427,908.572 				"/>
				<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="1380.394,908.572 1380.394,999.246 
					1389.285,999.246 1389.285,1033.756 1430.771,1033.756 1430.771,999.246 1439.66,999.246 1439.66,908.572 				"/>
				<g>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1503.336,920.216
						c-18.083,0-32.729,25.387-32.729,56.693h65.455C1536.062,945.603,1521.402,920.216,1503.336,920.216z"/>
				</g>
				<g>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1503.336,931.607
						c-14.508,0-26.259,16.97-26.259,37.905h52.514C1529.591,948.577,1517.83,931.607,1503.336,931.607z"/>
				</g>
				<g>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1606.866,922.206
						c-18.082,0-32.729,25.384-32.729,56.702h65.457C1639.593,947.59,1624.931,922.206,1606.866,922.206z"/>
				</g>
				<g>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1606.866,933.596
						c-14.507,0-26.259,16.978-26.259,37.904h52.516C1633.123,950.573,1621.361,933.596,1606.866,933.596z"/>
				</g>
				<g>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1706.083,920.216
						c-18.084,0-32.73,25.387-32.73,56.693h65.455C1738.808,945.603,1724.148,920.216,1706.083,920.216z"/>
				</g>
				<g>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1706.083,931.607
						c-14.51,0-26.26,16.97-26.26,37.905h52.514C1732.337,948.577,1720.577,931.607,1706.083,931.607z"/>
				</g>
				<g>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1791.794,957.414
						c-12.332,0-22.322,10.836-22.322,24.188h44.641C1814.113,968.25,1804.117,957.414,1791.794,957.414z"/>
				</g>
				<g>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1410.03,957.414
						c-12.333,0-22.323,10.836-22.323,24.188h44.641C1432.347,968.25,1422.351,957.414,1410.03,957.414z"/>
				</g>
				
					<rect x="1387.707" y="922.206" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="44.641" height="26.354"/>
				
					<rect x="1769.472" y="920.42" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="44.641" height="26.354"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1477.078" y1="843.951" x2="1445.1" y2="908.572"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1723.335" y1="843.951" x2="1755.689" y2="908.572"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1606.865" y1="843.951" x2="1606.865" y2="908.572"/>
			</g>
			<g>
				<g>
					
						<rect x="1518.433" y="374.832" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="166.078" height="469.119"/>
					
						<rect x="1535.688" y="358.658" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="133.726" height="16.174"/>
					
						<rect x="1554.021" y="279.933" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="100.294" height="55.593"/>
					
						<rect x="1569.12" y="197.972" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="72.255" height="81.961"/>
					
						<rect x="1593.923" y="197.972" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.804" height="81.961"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M1619.27,171.98
						c0.132-0.949,0.212-1.923,0.212-2.924c0-8.792-5.398-15.919-12.057-15.919c-6.66,0-12.057,7.127-12.057,15.919
						c0,1,0.08,1.975,0.213,2.924h-9.855v25.991h41.469V171.98H1619.27z"/>
					
						<rect x="1606.46" y="97.618" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="1.23" height="55.519"/>
					<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="1590.153,335.594 1523.433,335.746 
						1546.855,358.658 1604.169,358.527 1661.483,358.658 1684.906,335.749 1618.185,335.594 1618.121,335.526 1604.123,335.56 
						1590.218,335.526 					"/>
				</g>
				<g>
					
						<rect x="1538.384" y="695.126" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.725" height="53.922"/>
					
						<rect x="1572.894" y="695.126" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.725" height="53.922"/>
					
						<rect x="1608.482" y="695.126" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.727" height="53.922"/>
					
						<rect x="1642.992" y="695.126" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.727" height="53.922"/>
					
						<rect x="1538.384" y="764.146" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.725" height="53.923"/>
					
						<rect x="1572.894" y="764.146" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.725" height="53.923"/>
					
						<rect x="1608.482" y="764.146" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.727" height="53.923"/>
					
						<rect x="1642.992" y="764.146" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.727" height="53.923"/>
				</g>
				<g>
					
						<rect x="1538.923" y="423.362" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1573.433" y="423.362" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1609.021" y="423.362" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1643.531" y="423.362" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1538.923" y="492.382" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1573.433" y="492.382" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1609.021" y="492.382" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1643.531" y="492.382" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
				</g>
				<g>
					
						<rect x="1538.923" y="558.166" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1573.433" y="558.166" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1609.021" y="558.166" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1643.531" y="558.166" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1538.923" y="627.186" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1573.433" y="627.186" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1609.021" y="627.186" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1643.531" y="627.186" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="53.922"/>
				</g>
			</g>
		</g>
		<g id="base2_7_">
			<g>
				<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M80.613,976.084
					c-18.265,0-33.073,13.195-33.073,29.473h66.145C113.685,989.279,98.877,976.084,80.613,976.084z"/>
				<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M199.41,976.084
					c-18.266,0-33.074,13.202-33.074,29.479h66.145C232.48,989.286,217.674,976.084,199.41,976.084z"/>
				<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M324.28,976.084
					c-18.265,0-33.073,13.202-33.073,29.479h66.145C357.352,989.286,342.544,976.084,324.28,976.084z"/>
				<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M449.379,976.084
					c-18.267,0-33.074,13.202-33.074,29.479h66.146C482.45,989.286,467.643,976.084,449.379,976.084z"/>
				<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M572.216,976.084
					c-18.265,0-33.073,13.195-33.073,29.473h66.145C605.288,989.279,590.48,976.084,572.216,976.084z"/>
				<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2650.072,974.467
					c-18.266,0-33.076,13.201-33.076,29.479h66.145C2683.14,987.668,2668.339,974.467,2650.072,974.467z"/>
				<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2775.281,974.467
					c-18.268,0-33.078,13.201-33.078,29.479h66.148C2808.351,987.668,2793.546,974.467,2775.281,974.467z"/>
				<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2899.646,974.467
					c-18.266,0-33.078,13.201-33.078,29.479h66.146C2932.714,987.668,2917.91,974.467,2899.646,974.467z"/>
				<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M3024.744,974.467
					c-18.266,0-33.076,13.201-33.076,29.479h66.145C3057.812,987.668,3043.011,974.467,3024.744,974.467z"/>
				<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M3147.72,974.467
					c-18.268,0-33.078,13.201-33.078,29.479h66.145C3180.787,987.668,3165.984,974.467,3147.72,974.467z"/>
				<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M3271.218,974.467
					c-18.268,0-33.08,13.201-33.08,29.479h66.146C3304.285,987.668,3289.482,974.467,3271.218,974.467z"/>
				<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M3398.261,974.467
					c-18.268,0-33.078,13.201-33.078,29.479h66.148C3431.332,987.668,3416.527,974.467,3398.261,974.467z"/>
				<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M3521.226,974.467
					c-18.264,0-33.076,13.201-33.076,29.479h66.146C3554.296,987.668,3539.494,974.467,3521.226,974.467z"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="2562.357" y1="961.164" x2="3129.074" y2="961.164"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3129.074" y1="961.164" x2="3622.996" y2="961.164"/>
				
					<rect x="3622.996" y="919.643" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="259.363" height="84.304"/>
				
					<rect x="3644.027" y="944.987" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="238.332" height="58.959"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3666.132" y1="1003.946" x2="3709.81" y2="961.794"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3709.81" y1="961.794" x2="3882.359" y2="961.794"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3709.81" y1="1003.946" x2="3732.458" y2="982.868"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3732.458" y1="982.868" x2="3882.359" y2="982.868"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3752.677" y1="982.868" x2="3752.677" y2="1003.946"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3793.388" y1="982.868" x2="3793.388" y2="1003.946"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3832.212" y1="982.868" x2="3832.212" y2="1003.946"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3871.576" y1="982.868" x2="3871.576" y2="1003.946"/>
			</g>
			<rect y="941.752" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="631.962" height="19.412"/>
		</g>
		<g id="base1_7_">
			<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="0" y1="1005.38" x2="631.962" y2="1005.38"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="0" y1="1026.948" x2="1345.885" y2="1026.948"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3882.359" y1="1026.948" x2="2562.357" y2="1026.948"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="2562.357" y1="1026.948" x2="1848.435" y2="1026.948"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="2562.357" y1="1003.946" x2="3882.359" y2="1003.946"/>
		</g>
		<g id="shadow2_7_">
			
				<rect x="350.851" y="918.977" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="165.359" height="21.873"/>
			<g>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="517.725" x2="407.648" y2="517.725"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="533.54" x2="407.648" y2="533.54"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="550.794" x2="407.648" y2="550.794"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="565.892" x2="407.648" y2="565.892"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="581.716" x2="407.648" y2="581.716"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="598.97" x2="407.648" y2="598.97"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="614.059" x2="407.648" y2="614.059"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="629.883" x2="407.648" y2="629.883"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="647.137" x2="407.648" y2="647.137"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="662.234" x2="407.648" y2="662.234"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="678.05" x2="407.648" y2="678.05"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="695.304" x2="407.648" y2="695.304"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="709.324" x2="407.648" y2="709.324"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="725.146" x2="407.648" y2="725.146"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="742.402" x2="407.648" y2="742.402"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="757.5" x2="407.648" y2="757.5"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="773.313" x2="407.648" y2="773.313"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="790.569" x2="407.648" y2="790.569"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="386.08" y1="805.304" x2="408.366" y2="805.304"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="386.08" y1="821.126" x2="408.366" y2="821.126"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="386.08" y1="838.382" x2="408.366" y2="838.382"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="386.08" y1="853.479" x2="408.366" y2="853.479"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="386.08" y1="869.295" x2="408.366" y2="869.295"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="386.08" y1="886.549" x2="408.366" y2="886.549"/>
			</g>
			<g>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="517.725" x2="466.602" y2="517.725"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="533.54" x2="466.602" y2="533.54"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="550.794" x2="466.602" y2="550.794"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="565.892" x2="466.602" y2="565.892"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="581.716" x2="466.602" y2="581.716"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="598.97" x2="466.602" y2="598.97"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="614.059" x2="466.602" y2="614.059"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="629.883" x2="466.602" y2="629.883"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="647.137" x2="466.602" y2="647.137"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="662.234" x2="466.602" y2="662.234"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="678.05" x2="466.602" y2="678.05"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="695.304" x2="466.602" y2="695.304"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="709.324" x2="466.602" y2="709.324"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="725.146" x2="466.602" y2="725.146"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="742.402" x2="466.602" y2="742.402"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="757.5" x2="466.602" y2="757.5"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="773.313" x2="466.602" y2="773.313"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="790.569" x2="466.602" y2="790.569"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="445.033" y1="805.304" x2="467.32" y2="805.304"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="445.033" y1="821.126" x2="467.32" y2="821.126"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="445.033" y1="838.382" x2="467.32" y2="838.382"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="445.033" y1="853.479" x2="467.32" y2="853.479"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="445.033" y1="869.295" x2="467.32" y2="869.295"/>
				
					<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="445.033" y1="886.549" x2="467.32" y2="886.549"/>
			</g>
			
				<rect x="444.315" y="431.827" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="1.901" height="16.718"/>
			<g>
				
					<rect x="446.217" y="471.731" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="20.385" height="19.951"/>
				
					<rect x="416.56" y="431.827" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="1.618" height="16.718"/>
				
					<rect x="428.962" y="431.827" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="1.617" height="16.718"/>
			</g>
		</g>
		<g id="build3_8_">
			
				<rect x="184.05" y="726.048" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="148.105" height="215.704"/>
			<g display="none">
				
					<line display="inline" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1063.95" y1="585.838" x2="1025.469" y2="622.25"/>
				
					<line display="inline" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1063.95" y1="585.838" x2="1102.43" y2="622.25"/>
				
					<line display="inline" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1063.95" y1="594.8" x2="1038.827" y2="618.616"/>
				
					<line display="inline" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1038.827" y1="618.616" x2="1088.141" y2="618.616"/>
				
					<line display="inline" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1088.141" y1="618.616" x2="1063.95" y2="594.8"/>
				
					<rect x="1057.039" y="606.708" display="inline" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="15.411" height="9.386"/>
				
					<line display="inline" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1074.131" y1="591.997" x2="1081.416" y2="591.997"/>
				
					<line display="inline" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1081.416" y1="591.997" x2="1081.416" y2="603.205"/>
			</g>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="277.696" y1="667.829" x2="277.696" y2="673.221"/>
			<g>
				<g>
					
						<rect x="209.756" y="749.79" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="26.421" height="45.834"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="209.756" y1="772.707" x2="236.177" y2="772.707"/>
				</g>
				<g>
					<g>
						
							<rect x="253.433" y="749.79" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="66.323" height="45.834"/>
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="270.688" y1="749.79" x2="270.688" y2="795.624"/>
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="304.657" y1="749.79" x2="304.657" y2="795.624"/>
					</g>
					<g>
						
							<rect x="253.433" y="813.418" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="66.323" height="45.834"/>
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="270.688" y1="813.418" x2="270.688" y2="859.252"/>
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="304.657" y1="813.418" x2="304.657" y2="859.252"/>
					</g>
					<g>
						
							<rect x="253.433" y="884.981" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="66.323" height="45.834"/>
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="270.688" y1="884.981" x2="270.688" y2="930.815"/>
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="304.657" y1="884.981" x2="304.657" y2="930.815"/>
					</g>
				</g>
				<g>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M223.594,822.77
						c-9.927,0-17.973,9.674-17.973,21.61v10.7v21.611h35.948V855.08v-10.7C241.569,832.443,233.523,822.77,223.594,822.77z"/>
					<path fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M236.177,856.815
						c0,1.349-0.828,2.437-1.85,2.437c-1.019,0-1.847-1.088-1.847-2.437c0-1.346,0.828-2.434,1.847-2.434
						C235.35,854.382,236.177,855.47,236.177,856.815z"/>
					
						<rect x="205.621" y="876.691" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="35.948" height="65.061"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="205.621" y1="881.723" x2="241.569" y2="881.723"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="205.621" y1="887.451" x2="241.569" y2="887.451"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="205.621" y1="893.221" x2="241.569" y2="893.221"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="205.621" y1="897.536" x2="241.569" y2="897.536"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="205.621" y1="901.851" x2="241.569" y2="901.851"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="205.621" y1="907.898" x2="241.569" y2="907.898"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="205.621" y1="912.55" x2="241.569" y2="912.55"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="205.621" y1="916.948" x2="241.569" y2="916.948"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="204.993" y1="924.193" x2="240.942" y2="924.193"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="205.621" y1="930.815" x2="241.569" y2="930.815"/>
				</g>
			</g>
			<g>
				<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="292.502,684.14 292.502,660.339 
					280.553,660.339 280.553,671.558 261.02,650.979 185.007,725.644 332.43,726.199 				"/>
				<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="212.864,711.636 304.571,711.986 
					260.147,665.188 				"/>
				
					<rect x="243.908" y="688.59" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="31.015" height="17.731"/>
			</g>
		</g>
		<g id="build4_8_">
			<g>
				<g>
					
						<rect x="56.841" y="714.201" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="72.571" height="49.609"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="72.255" y1="714.201" x2="72.255" y2="763.811"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="113.685" y1="714.201" x2="113.685" y2="763.811"/>
				</g>
				<g>
					
						<rect x="56.841" y="791.469" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.771" height="53.225"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="56.841" y1="818.085" x2="80.613" y2="818.085"/>
				</g>
				<g>
					
						<rect x="101.798" y="791.469" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.773" height="53.225"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="101.798" y1="818.085" x2="125.571" y2="818.085"/>
				</g>
				<g>
					
						<rect x="56.841" y="870.928" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.771" height="53.225"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="56.841" y1="897.536" x2="80.613" y2="897.536"/>
				</g>
				<g>
					
						<rect x="101.798" y="870.928" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.773" height="53.225"/>
					
						<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="101.798" y1="897.536" x2="125.571" y2="897.536"/>
				</g>
			</g>
			<g>
				<rect y="689.397" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="144.51" height="252.354"/>
			</g>
			<g>
				<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="105.404,639.608 105.404,611.086 
					93.358,611.086 93.358,624.533 73.666,599.871 -2.957,689.339 145.652,690.004 				"/>
				<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="25.127,672.556 117.569,672.974 
					72.791,616.899 				"/>
				
					<rect x="56.418" y="644.938" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="31.267" height="21.249"/>
			</g>
		</g>
		<g id="build6_8_">
			
				<rect x="510.112" y="893.303" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="7.217" height="96.229"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="517.329" y1="893.303" x2="510.112" y2="886.244"/>
		</g>
		<g>
			<g>
				<g id="build11_7_">
					<g id="build5_8_">
						
							<rect x="498.956" y="534.439" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="113.595" height="281.135"/>
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="516.21" y1="575.085" x2="516.21" y2="815.574"/>
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="592.061" y1="575.085" x2="592.061" y2="815.574"/>
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="579.119" y1="511.457" x2="579.119" y2="534.439"/>
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="564.021" y1="518.626" x2="564.021" y2="534.439"/>
					</g>
					<g id="build7_8_">
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="612.551" y1="815.574" x2="732.256" y2="815.574"/>
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="732.256" y1="815.574" x2="732.256" y2="859.252"/>
						
							<rect x="516.21" y="828.516" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.933" height="21.214"/>
						
							<rect x="564.021" y="828.516" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.647" height="21.214"/>
						
							<rect x="599.609" y="828.516" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.646" height="21.214"/>
						
							<rect x="641.365" y="828.516" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="20.793" height="21.214"/>
						
							<rect x="679.414" y="828.516" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.726" height="21.214"/>
						
							<rect x="516.21" y="870.195" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.933" height="23.025"/>
						
							<rect x="564.021" y="870.928" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.647" height="22.293"/>
						
							<rect x="599.609" y="870.928" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.646" height="22.293"/>
						
							<rect x="539.144" y="909.221" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="92.818" height="32.531"/>
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="539.144" y1="916.948" x2="631.962" y2="916.948"/>
					</g>
					<g id="build10_7_">
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="612.551" y1="650.035" x2="842.705" y2="650.035"/>
						<g>
							
								<rect x="631.962" y="705.17" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="77.648" height="27.364"/>
							
								<rect x="631.962" y="758.417" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="77.648" height="24.804"/>
							
								<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="672.404" y1="705.17" x2="672.404" y2="732.534"/>
							
								<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="672.404" y1="758.402" x2="672.404" y2="783.221"/>
						</g>
					</g>
					<g id="build9_7_">
						
							<rect x="732.256" y="660.01" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="338.628" height="199.039"/>
						<g>
							
								<rect x="732.256" y="660.01" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="90.589" height="36.114"/>
							
								<rect x="822.845" y="660.01" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="36.667" height="36.114"/>
							
								<rect x="859.512" y="660.01" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="211.373" height="36.114"/>
							
								<rect x="732.256" y="696.053" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="214.609" height="42.771"/>
							
								<rect x="946.865" y="696.124" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="37.745" height="42.699"/>
							
								<rect x="984.61" y="696.124" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="86.274" height="42.699"/>
							
								<rect x="732.256" y="738.823" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="252.354" height="35.127"/>
							
								<rect x="984.61" y="738.823" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="43.138" height="35.127"/>
							
								<rect x="1027.748" y="738.823" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="43.137" height="35.127"/>
							
								<rect x="732.256" y="773.95" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="338.628" height="41.264"/>
							
								<rect x="859.512" y="773.95" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="42.059" height="41.264"/>
							
								<rect x="777.551" y="815.214" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="39.126" height="43.835"/>
							
								<rect x="959.807" y="815.214" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="40.98" height="43.835"/>
						</g>
					</g>
					<g id="buld8_7_">
						
							<rect x="842.705" y="354.008" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="186.121" height="306.002"/>
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="984.61" y1="343.223" x2="984.61" y2="354.008"/>
						
							<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1006.179" y1="319.497" x2="1006.179" y2="354.008"/>
						<g>
							
								<rect x="842.705" y="405.772" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="186.121" height="17.255"/>
							
								<rect x="842.705" y="463.255" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="185.043" height="19.051"/>
							
								<rect x="842.705" y="526.528" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="186.121" height="19.438"/>
							
								<rect x="842.705" y="582.615" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="186.121" height="17.256"/>
							
								<rect x="880.541" y="641.946" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="118.089" height="18.063"/>
							
								<rect x="880.541" y="405.772" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="49.069" height="17.255"/>
							
								<rect x="965.198" y="405.772" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="40.98" height="17.255"/>
							
								<rect x="864.904" y="463.255" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="44.216" height="19.051"/>
							
								<rect x="946.865" y="463.255" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="43.138" height="19.051"/>
							
								<rect x="880.541" y="526.528" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="43.676" height="19.438"/>
							
								<rect x="965.198" y="526.528" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="45.295" height="19.438"/>
							
								<rect x="864.904" y="582.615" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="44.216" height="17.256"/>
							
								<rect x="952.258" y="582.615" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="46.372" height="17.256"/>
						</g>
					</g>
					
						<rect x="1287.726" y="433.163" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="82.167" height="426.199"/>
					
						<rect x="1113.412" y="314.434" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="174.314" height="424.657"/>
					
						<rect x="1070.885" y="739.005" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="216.841" height="120.247"/>
					
						<rect x="1259.808" y="314.433" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="27.918" height="424.431"/>
					
						<rect x="1113.412" y="314.433" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="31.816" height="424.658"/>
					<g>
						
							<rect x="1219.858" y="346.897" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.548" height="23.523"/>
						
							<rect x="1157.872" y="346.898" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.547" height="23.524"/>
						
							<rect x="1218.997" y="394.349" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.548" height="23.523"/>
						
							<rect x="1157.01" y="394.354" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.546" height="23.523"/>
						
							<rect x="1220.479" y="432.974" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.547" height="23.523"/>
						
							<rect x="1158.493" y="432.979" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.547" height="23.523"/>
						
							<rect x="1219.618" y="480.43" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.547" height="23.523"/>
						
							<rect x="1157.632" y="480.435" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.547" height="23.523"/>
					</g>
					<g>
						
							<rect x="1219.115" y="521.401" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.546" height="23.523"/>
						
							<rect x="1157.128" y="521.406" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.547" height="23.523"/>
						
							<rect x="1218.253" y="568.857" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.546" height="23.523"/>
						
							<rect x="1156.266" y="568.861" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.547" height="23.523"/>
						
							<rect x="1219.736" y="607.482" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.547" height="23.523"/>
						
							<rect x="1157.749" y="607.487" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.548" height="23.522"/>
						
							<rect x="1218.875" y="654.938" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.547" height="23.522"/>
						
							<rect x="1156.888" y="654.942" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.548" height="23.523"/>
					</g>
					
						<rect x="1070.885" y="828.516" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="216.841" height="8.896"/>
					
						<rect x="1070.885" y="843.951" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="216.841" height="5.778"/>
					
						<rect x="1088.005" y="758.402" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="25.883" height="24.818"/>
					
						<rect x="1158.493" y="758.402" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.547" height="24.818"/>
					
						<rect x="1232.753" y="758.417" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.813" height="24.804"/>
					
						<rect x="1314.412" y="417.056" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0" height="15.676"/>
				</g>
				<g>
					
						<rect x="612.551" y="575.085" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="230.154" height="74.95"/>
					
						<rect x="612.551" y="631.01" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="230.154" height="19.025"/>
					
						<rect x="649.397" y="550.794" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="193.308" height="24.291"/>
					
						<rect x="659.229" y="592.381" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.724" height="15.106"/>
					
						<rect x="704.853" y="589.437" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.724" height="15.106"/>
					
						<rect x="749.445" y="592.381" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.724" height="15.106"/>
					
						<rect x="795.07" y="589.437" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.723" height="15.106"/>
				</g>
			</g>
			<g>
				
					<rect x="1312.542" y="502.366" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="33.343" height="356.995"/>
				
					<rect x="1328.81" y="522.949" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="0" height="336.412"/>
				
					<rect x="1301.775" y="450.322" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="20.132" height="21.409"/>
				
					<rect x="1338.425" y="450.322" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="24.714" height="21.409"/>
			</g>
		</g>
		
			<rect x="1387.707" y="725.146" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="130.729" height="118.805"/>
		
			<rect x="1387.707" y="764.146" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="130.727" height="34.983"/>
		
			<rect x="1387.707" y="725.146" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="44.641" height="118.805"/>
		
			<rect x="1477.078" y="725.146" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="41.357" height="118.805"/>
		
			<rect x="56.841" y="714.201" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="72.571" height="49.609"/>
		
			<rect x="72.255" y="714.201" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="41.429" height="49.609"/>
		
			<rect x="56.841" y="791.469" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.771" height="53.225"/>
		
			<rect x="101.798" y="791.469" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.773" height="53.225"/>
		
			<rect x="56.841" y="818.085" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.771" height="0"/>
		
			<rect x="101.798" y="818.085" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.773" height="0"/>
		
			<rect x="56.841" y="870.928" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.771" height="53.225"/>
		
			<rect x="101.798" y="870.928" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.773" height="53.225"/>
		
			<rect x="101.798" y="897.536" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.773" height="0"/>
		
			<rect x="56.841" y="897.536" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="23.771" height="0"/>
		
			<rect x="1387.707" y="764.146" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="44.641" height="34.224"/>
		
			<rect x="1477.078" y="763.811" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="41.355" height="35.318"/>
	</g>
	<g>
		<g id="build1_8_">
			<g>
				
					<rect x="631.96" y="893.559" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="713.923" height="118.629"/>
				
					<rect x="1848.433" y="893.559" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="713.924" height="118.629"/>
				<g>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M685.882,928.819
						c-1.809-9.101-9.572-15.933-18.875-15.933c-9.303,0-17.068,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H685.882z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M735.598,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.303,0-17.068,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H735.598z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M785.599,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.304,0-17.068,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H785.599z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M835.315,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.305,0-17.068,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H835.315z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M885.316,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.304,0-17.068,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H885.316z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M935.032,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.304,0-17.067,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H935.032z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M985.034,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.304,0-17.068,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H985.034z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1034.75,928.819
						c-1.81-9.101-9.574-15.933-18.875-15.933c-9.305,0-17.068,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H1034.75z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1084.751,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.303,0-17.067,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H1084.751z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1134.466,928.819
						c-1.81-9.101-9.573-15.933-18.874-15.933c-9.304,0-17.067,6.832-18.877,15.933h-3.731v24.053h45.689v-24.053H1134.466z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1184.468,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.303,0-17.067,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H1184.468z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1234.183,928.819
						c-1.81-9.101-9.573-15.933-18.875-15.933c-9.303,0-17.066,6.832-18.876,15.933h-3.73v24.053h45.687v-24.053H1234.183z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1283.334,928.819
						c-1.809-9.101-9.573-15.933-18.875-15.933c-9.303,0-17.066,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H1283.334z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1333.05,928.819
						c-1.809-9.101-9.574-15.933-18.875-15.933c-9.303,0-17.067,6.832-18.877,15.933h-3.73v24.053h45.688v-24.053H1333.05z"/>
				</g>
				<g>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1900.449,924.504
						c-1.809-9.097-9.574-15.932-18.875-15.932c-9.303,0-17.067,6.835-18.877,15.932h-3.73v24.056h45.689v-24.056H1900.449z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1950.169,924.504
						c-1.813-9.097-9.58-15.932-18.879-15.932c-9.307,0-17.066,6.835-18.877,15.932h-3.73v24.056h45.689v-24.056H1950.169z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2000.164,924.504
						c-1.803-9.097-9.57-15.932-18.871-15.932c-9.303,0-17.07,6.835-18.873,15.932h-3.732v24.056h45.691v-24.056H2000.164z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2049.88,924.504
						c-1.811-9.097-9.57-15.932-18.871-15.932s-17.07,6.835-18.883,15.932h-3.723v24.056h45.682v-24.056H2049.88z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2099.884,924.504
						c-1.811-9.097-9.57-15.932-18.871-15.932c-9.311,0-17.07,6.835-18.881,15.932h-3.732v24.056h45.689v-24.056H2099.884z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2149.603,924.504
						c-1.813-9.097-9.578-15.932-18.883-15.932c-9.301,0-17.061,6.835-18.871,15.932h-3.732v24.056h45.689v-24.056H2149.603z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2199.599,924.504
						c-1.805-9.097-9.57-15.932-18.873-15.932s-17.068,6.835-18.873,15.932h-3.732v24.056h45.689v-24.056H2199.599z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2249.316,924.504
						c-1.811-9.097-9.57-15.932-18.873-15.932c-9.301,0-17.068,6.835-18.881,15.932h-3.725v24.056h45.684v-24.056H2249.316z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2299.32,924.504
						c-1.811-9.097-9.57-15.932-18.873-15.932c-9.309,0-17.068,6.835-18.879,15.932h-3.732v24.056h45.689v-24.056H2299.32z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2349.039,924.504
						c-1.813-9.097-9.58-15.932-18.883-15.932c-9.301,0-17.061,6.835-18.871,15.932h-3.732v24.056h45.689v-24.056H2349.039z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2399.033,924.504
						c-1.803-9.097-9.57-15.932-18.871-15.932c-9.303,0-17.07,6.835-18.873,15.932h-3.732v24.056h45.688v-24.056H2399.033z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2448.75,924.504
						c-1.811-9.097-9.57-15.932-18.871-15.932s-17.07,6.835-18.883,15.932h-3.723v24.056h45.682v-24.056H2448.75z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2497.904,924.504
						c-1.813-9.097-9.572-15.932-18.873-15.932c-9.311,0-17.07,6.835-18.881,15.932h-3.732v24.056h45.689v-24.056H2497.904z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2547.621,924.504
						c-1.813-9.097-9.578-15.932-18.883-15.932c-9.301,0-17.059,6.835-18.871,15.932h-3.732v24.056h45.689v-24.056H2547.621z"/>
				</g>
				
					<rect x="1345.883" y="908.572" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="502.55" height="125.184"/>
				<g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M667.246,961.501
							c-14.294,0-25.883,22.698-25.883,50.687h51.765C693.127,984.199,681.539,961.501,667.246,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M716.958,961.501
							c-14.293,0-25.881,22.698-25.881,50.687h51.765C742.841,984.199,731.252,961.501,716.958,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M766.961,961.501
							c-14.294,0-25.883,22.698-25.883,50.687h51.765C792.843,984.199,781.254,961.501,766.961,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M816.821,961.501
							c-14.293,0-25.882,22.698-25.882,50.687h51.765C842.704,984.199,831.114,961.501,816.821,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M866.535,961.501
							c-14.294,0-25.883,22.698-25.883,50.687h51.765C892.417,984.199,880.828,961.501,866.535,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M916.536,961.501
							c-14.293,0-25.883,22.698-25.883,50.687h51.766C942.419,984.199,930.829,961.501,916.536,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M964.312,961.501
							c-14.293,0-25.882,22.698-25.882,50.687h51.765C990.194,984.199,978.605,961.501,964.312,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1014.025,961.501
							c-14.293,0-25.883,22.698-25.883,50.687h51.765C1039.907,984.199,1028.318,961.501,1014.025,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1064.026,961.501
							c-14.293,0-25.882,22.698-25.882,50.687h51.765C1089.909,984.199,1078.321,961.501,1064.026,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1113.888,961.501
							c-14.294,0-25.883,22.698-25.883,50.687h51.765C1139.77,984.199,1128.181,961.501,1113.888,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1163.601,961.501
							c-14.293,0-25.882,22.698-25.882,50.687h51.765C1189.483,984.199,1177.895,961.501,1163.601,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1213.603,961.501
							c-14.294,0-25.883,22.698-25.883,50.687h51.765C1239.485,984.199,1227.896,961.501,1213.603,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1262.541,961.501
							c-14.293,0-25.882,22.698-25.882,50.687h51.765C1288.423,984.199,1276.835,961.501,1262.541,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1312.542,961.501
							c-14.293,0-25.882,22.698-25.882,50.687h51.765C1338.425,984.199,1326.836,961.501,1312.542,961.501z"/>
					</g>
				</g>
				<g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1882.746,961.501
							c-14.293,0-25.883,22.698-25.883,50.687h51.766C1908.628,984.199,1897.04,961.501,1882.746,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1932.457,961.501
							c-14.289,0-25.881,22.698-25.881,50.687h51.766C1958.341,984.199,1946.757,961.501,1932.457,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1982.464,961.501
							c-14.299,0-25.883,22.698-25.883,50.687h51.762C2008.343,984.199,1996.751,961.501,1982.464,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2032.322,961.501
							c-14.295,0-25.883,22.698-25.883,50.687h51.768C2058.207,984.199,2046.613,961.501,2032.322,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2082.033,961.501
							c-14.289,0-25.883,22.698-25.883,50.687h51.764C2107.914,984.199,2096.33,961.501,2082.033,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2132.037,961.501
							c-14.289,0-25.883,22.698-25.883,50.687h51.766C2157.919,984.199,2146.333,961.501,2132.037,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2179.816,961.501
							c-14.299,0-25.883,22.698-25.883,50.687h51.766C2205.699,984.199,2194.107,961.501,2179.816,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2229.525,961.501
							c-14.289,0-25.881,22.698-25.881,50.687h51.764C2255.408,984.199,2243.824,961.501,2229.525,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2279.529,961.501
							c-14.297,0-25.881,22.698-25.881,50.687h51.764C2305.412,984.199,2293.818,961.501,2279.529,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2329.39,961.501
							c-14.299,0-25.883,22.698-25.883,50.687h51.766C2355.273,984.199,2343.679,961.501,2329.39,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2379.107,961.501
							c-14.297,0-25.881,22.698-25.881,50.687h51.764C2404.99,984.199,2393.396,961.501,2379.107,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2429.103,961.501
							c-14.289,0-25.883,22.698-25.883,50.687h51.764C2454.984,984.199,2443.4,961.501,2429.103,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2478.046,961.501
							c-14.299,0-25.885,22.698-25.885,50.687h51.768C2503.929,984.199,2492.335,961.501,2478.046,961.501z"/>
					</g>
					<g>
						<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2528.048,961.501
							c-14.299,0-25.883,22.698-25.883,50.687h51.766C2553.931,984.199,2542.339,961.501,2528.048,961.501z"/>
					</g>
				</g>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="631.96" y1="893.559" x2="657.843" y2="859.049"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="1345.883" y1="893.559" x2="1380.394" y2="843.951"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="1380.394" y1="843.951" x2="1809.611" y2="843.951"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="1809.611" y1="843.951" x2="1848.433" y2="893.559"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="657.843" y1="859.049" x2="1369.891" y2="859.049"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="1821.427" y1="859.049" x2="2528.984" y2="859.049"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="2528.984" y1="859.049" x2="2562.357" y2="893.559"/>
			</g>
			<g>
				<polygon fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" points="1460.198,908.572 1460.198,999.246 
					1473.139,999.246 1473.139,1033.756 1533.531,1033.756 1533.531,999.246 1546.472,999.246 1546.472,908.572 				"/>
				<polygon fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" points="1563.727,908.572 1563.727,999.246 
					1576.668,999.246 1576.668,1033.756 1637.06,1033.756 1637.06,999.246 1650.001,999.246 1650.001,908.572 				"/>
				<polygon fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" points="1662.943,908.572 1662.943,999.246 
					1675.884,999.246 1675.884,1033.756 1736.276,1033.756 1736.276,999.246 1749.218,999.246 1749.218,908.572 				"/>
				<polygon fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" points="1762.16,908.572 1762.16,999.246 
					1771.05,999.246 1771.05,1033.756 1812.537,1033.756 1812.537,999.246 1821.427,999.246 1821.427,908.572 				"/>
				<polygon fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" points="1380.394,908.572 1380.394,999.246 
					1389.285,999.246 1389.285,1033.756 1430.771,1033.756 1430.771,999.246 1439.66,999.246 1439.66,908.572 				"/>
				<g>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1503.336,920.216
						c-18.083,0-32.729,25.387-32.729,56.693h65.455C1536.062,945.603,1521.402,920.216,1503.336,920.216z"/>
				</g>
				<g>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1503.336,931.607
						c-14.508,0-26.259,16.97-26.259,37.905h52.514C1529.591,948.577,1517.83,931.607,1503.336,931.607z"/>
				</g>
				<g>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1606.866,922.206
						c-18.082,0-32.729,25.384-32.729,56.702h65.457C1639.593,947.59,1624.931,922.206,1606.866,922.206z"/>
				</g>
				<g>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1606.866,933.596
						c-14.507,0-26.259,16.978-26.259,37.904h52.516C1633.123,950.573,1621.361,933.596,1606.866,933.596z"/>
				</g>
				<g>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1706.083,920.216
						c-18.084,0-32.73,25.387-32.73,56.693h65.455C1738.808,945.603,1724.148,920.216,1706.083,920.216z"/>
				</g>
				<g>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1706.083,931.607
						c-14.51,0-26.26,16.97-26.26,37.905h52.514C1732.337,948.577,1720.577,931.607,1706.083,931.607z"/>
				</g>
				<g>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1791.794,957.414
						c-12.332,0-22.322,10.836-22.322,24.188h44.641C1814.113,968.25,1804.117,957.414,1791.794,957.414z"/>
				</g>
				<g>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1410.03,957.414
						c-12.333,0-22.323,10.836-22.323,24.188h44.641C1432.347,968.25,1422.351,957.414,1410.03,957.414z"/>
				</g>
				
					<rect x="1387.707" y="922.206" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="44.641" height="26.354"/>
				
					<rect x="1769.472" y="920.42" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="44.641" height="26.354"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="1477.078" y1="843.951" x2="1445.1" y2="908.572"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="1723.335" y1="843.951" x2="1755.689" y2="908.572"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="1606.865" y1="843.951" x2="1606.865" y2="908.572"/>
			</g>
			<g>
				<g>
					
						<rect x="1518.433" y="374.832" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="166.078" height="469.119"/>
					
						<rect x="1535.688" y="358.658" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="133.726" height="16.174"/>
					
						<rect x="1554.021" y="279.933" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="100.294" height="55.593"/>
					
						<rect x="1569.12" y="197.972" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="72.255" height="81.961"/>
					
						<rect x="1593.923" y="197.972" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.804" height="81.961"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M1619.27,171.98
						c0.132-0.949,0.212-1.923,0.212-2.924c0-8.792-5.398-15.919-12.057-15.919c-6.66,0-12.057,7.127-12.057,15.919
						c0,1,0.08,1.975,0.213,2.924h-9.855v25.991h41.469V171.98H1619.27z"/>
					
						<rect x="1606.46" y="97.618" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="1.23" height="55.519"/>
					<polygon fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" points="1590.153,335.594 1523.433,335.746 
						1546.855,358.658 1604.169,358.527 1661.483,358.658 1684.906,335.749 1618.185,335.594 1618.121,335.526 1604.123,335.56 
						1590.218,335.526 					"/>
				</g>
				<g>
					
						<rect x="1538.384" y="695.126" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.725" height="53.922"/>
					
						<rect x="1572.894" y="695.126" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.725" height="53.922"/>
					
						<rect x="1608.482" y="695.126" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.727" height="53.922"/>
					
						<rect x="1642.992" y="695.126" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.727" height="53.922"/>
					
						<rect x="1538.384" y="764.146" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.725" height="53.923"/>
					
						<rect x="1572.894" y="764.146" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.725" height="53.923"/>
					
						<rect x="1608.482" y="764.146" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.727" height="53.923"/>
					
						<rect x="1642.992" y="764.146" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.727" height="53.923"/>
				</g>
				<g>
					
						<rect x="1538.923" y="423.362" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1573.433" y="423.362" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1609.021" y="423.362" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1643.531" y="423.362" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1538.923" y="492.382" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1573.433" y="492.382" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1609.021" y="492.382" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1643.531" y="492.382" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
				</g>
				<g>
					
						<rect x="1538.923" y="558.166" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1573.433" y="558.166" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1609.021" y="558.166" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1643.531" y="558.166" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1538.923" y="627.186" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1573.433" y="627.186" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1609.021" y="627.186" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
					
						<rect x="1643.531" y="627.186" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="53.922"/>
				</g>
			</g>
		</g>
		<g id="base2_6_">
			<g>
				<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M80.613,976.084
					c-18.265,0-33.073,13.195-33.073,29.473h66.145C113.685,989.279,98.877,976.084,80.613,976.084z"/>
				<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M199.41,976.084
					c-18.266,0-33.074,13.202-33.074,29.479h66.145C232.48,989.286,217.674,976.084,199.41,976.084z"/>
				<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M324.28,976.084
					c-18.265,0-33.073,13.202-33.073,29.479h66.145C357.352,989.286,342.544,976.084,324.28,976.084z"/>
				<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M449.379,976.084
					c-18.267,0-33.074,13.202-33.074,29.479h66.146C482.45,989.286,467.643,976.084,449.379,976.084z"/>
				<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M572.216,976.084
					c-18.265,0-33.073,13.195-33.073,29.473h66.145C605.288,989.279,590.48,976.084,572.216,976.084z"/>
				<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2650.072,974.467
					c-18.266,0-33.076,13.201-33.076,29.479h66.145C2683.14,987.668,2668.339,974.467,2650.072,974.467z"/>
				<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2775.281,974.467
					c-18.268,0-33.078,13.201-33.078,29.479h66.148C2808.351,987.668,2793.546,974.467,2775.281,974.467z"/>
				<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M2899.646,974.467
					c-18.266,0-33.078,13.201-33.078,29.479h66.146C2932.714,987.668,2917.91,974.467,2899.646,974.467z"/>
				<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M3024.744,974.467
					c-18.266,0-33.076,13.201-33.076,29.479h66.145C3057.812,987.668,3043.011,974.467,3024.744,974.467z"/>
				<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M3147.72,974.467
					c-18.268,0-33.078,13.201-33.078,29.479h66.145C3180.787,987.668,3165.984,974.467,3147.72,974.467z"/>
				<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M3271.218,974.467
					c-18.268,0-33.08,13.201-33.08,29.479h66.146C3304.285,987.668,3289.482,974.467,3271.218,974.467z"/>
				<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M3398.261,974.467
					c-18.268,0-33.078,13.201-33.078,29.479h66.148C3431.332,987.668,3416.527,974.467,3398.261,974.467z"/>
				<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M3521.226,974.467
					c-18.264,0-33.076,13.201-33.076,29.479h66.146C3554.296,987.668,3539.494,974.467,3521.226,974.467z"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="2562.357" y1="961.164" x2="3129.074" y2="961.164"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="3129.074" y1="961.164" x2="3622.996" y2="961.164"/>
				
					<rect x="3622.996" y="919.643" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="259.363" height="84.304"/>
				
					<rect x="3644.027" y="944.987" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="238.332" height="58.959"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="3666.132" y1="1003.946" x2="3709.81" y2="961.794"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="3709.81" y1="961.794" x2="3882.359" y2="961.794"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="3709.81" y1="1003.946" x2="3732.458" y2="982.868"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="3732.458" y1="982.868" x2="3882.359" y2="982.868"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="3752.677" y1="982.868" x2="3752.677" y2="1003.946"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="3793.388" y1="982.868" x2="3793.388" y2="1003.946"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="3832.212" y1="982.868" x2="3832.212" y2="1003.946"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="3871.576" y1="982.868" x2="3871.576" y2="1003.946"/>
			</g>
			<rect y="941.752" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="631.962" height="19.412"/>
		</g>
		<g id="base1_6_">
			<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="0" y1="1005.38" x2="631.962" y2="1005.38"/>
			
				<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="0" y1="1026.948" x2="1345.885" y2="1026.948"/>
			
				<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="3882.359" y1="1026.948" x2="2562.357" y2="1026.948"/>
			
				<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="2562.357" y1="1026.948" x2="1848.435" y2="1026.948"/>
			
				<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="2562.357" y1="1003.946" x2="3882.359" y2="1003.946"/>
		</g>
		<g id="shadow2_6_">
			
				<rect x="350.851" y="918.977" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="165.359" height="21.873"/>
			<g>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="517.725" x2="407.648" y2="517.725"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="533.54" x2="407.648" y2="533.54"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="550.794" x2="407.648" y2="550.794"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="565.892" x2="407.648" y2="565.892"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="581.716" x2="407.648" y2="581.716"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="598.97" x2="407.648" y2="598.97"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="614.059" x2="407.648" y2="614.059"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="629.883" x2="407.648" y2="629.883"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="647.137" x2="407.648" y2="647.137"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="662.234" x2="407.648" y2="662.234"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="678.05" x2="407.648" y2="678.05"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="695.304" x2="407.648" y2="695.304"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="709.324" x2="407.648" y2="709.324"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="725.146" x2="407.648" y2="725.146"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="742.402" x2="407.648" y2="742.402"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="757.5" x2="407.648" y2="757.5"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="773.313" x2="407.648" y2="773.313"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="385.361" y1="790.569" x2="407.648" y2="790.569"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="386.08" y1="805.304" x2="408.366" y2="805.304"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="386.08" y1="821.126" x2="408.366" y2="821.126"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="386.08" y1="838.382" x2="408.366" y2="838.382"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="386.08" y1="853.479" x2="408.366" y2="853.479"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="386.08" y1="869.295" x2="408.366" y2="869.295"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="386.08" y1="886.549" x2="408.366" y2="886.549"/>
			</g>
			<g>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="517.725" x2="466.602" y2="517.725"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="533.54" x2="466.602" y2="533.54"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="550.794" x2="466.602" y2="550.794"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="565.892" x2="466.602" y2="565.892"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="581.716" x2="466.602" y2="581.716"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="598.97" x2="466.602" y2="598.97"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="614.059" x2="466.602" y2="614.059"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="629.883" x2="466.602" y2="629.883"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="647.137" x2="466.602" y2="647.137"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="662.234" x2="466.602" y2="662.234"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="678.05" x2="466.602" y2="678.05"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="695.304" x2="466.602" y2="695.304"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="709.324" x2="466.602" y2="709.324"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="725.146" x2="466.602" y2="725.146"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="742.402" x2="466.602" y2="742.402"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="757.5" x2="466.602" y2="757.5"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="773.313" x2="466.602" y2="773.313"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="444.315" y1="790.569" x2="466.602" y2="790.569"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="445.033" y1="805.304" x2="467.32" y2="805.304"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="445.033" y1="821.126" x2="467.32" y2="821.126"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="445.033" y1="838.382" x2="467.32" y2="838.382"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="445.033" y1="853.479" x2="467.32" y2="853.479"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="445.033" y1="869.295" x2="467.32" y2="869.295"/>
				
					<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="445.033" y1="886.549" x2="467.32" y2="886.549"/>
			</g>
			
				<rect x="444.315" y="431.827" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="1.901" height="16.718"/>
			<g>
				
					<rect x="446.217" y="471.731" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="20.385" height="19.951"/>
				
					<rect x="416.56" y="431.827" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="1.618" height="16.718"/>
				
					<rect x="428.962" y="431.827" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="1.617" height="16.718"/>
			</g>
		</g>
		<g id="build3_7_">
			
				<rect x="184.05" y="726.048" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="148.105" height="215.704"/>
			<g display="none">
				
					<line display="inline" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="1063.95" y1="585.838" x2="1025.469" y2="622.25"/>
				
					<line display="inline" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="1063.95" y1="585.838" x2="1102.43" y2="622.25"/>
				
					<line display="inline" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="1063.95" y1="594.8" x2="1038.827" y2="618.616"/>
				
					<line display="inline" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="1038.827" y1="618.616" x2="1088.141" y2="618.616"/>
				
					<line display="inline" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="1088.141" y1="618.616" x2="1063.95" y2="594.8"/>
				
					<rect x="1057.039" y="606.708" display="inline" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="15.411" height="9.386"/>
				
					<line display="inline" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="1074.131" y1="591.997" x2="1081.416" y2="591.997"/>
				
					<line display="inline" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="1081.416" y1="591.997" x2="1081.416" y2="603.205"/>
			</g>
			
				<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="277.696" y1="667.829" x2="277.696" y2="673.221"/>
			<g>
				<g>
					
						<rect x="209.756" y="749.79" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="26.421" height="45.834"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="209.756" y1="772.707" x2="236.177" y2="772.707"/>
				</g>
				<g>
					<g>
						
							<rect x="253.433" y="749.79" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="66.323" height="45.834"/>
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="270.688" y1="749.79" x2="270.688" y2="795.624"/>
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="304.657" y1="749.79" x2="304.657" y2="795.624"/>
					</g>
					<g>
						
							<rect x="253.433" y="813.418" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="66.323" height="45.834"/>
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="270.688" y1="813.418" x2="270.688" y2="859.252"/>
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="304.657" y1="813.418" x2="304.657" y2="859.252"/>
					</g>
					<g>
						
							<rect x="253.433" y="884.981" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="66.323" height="45.834"/>
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="270.688" y1="884.981" x2="270.688" y2="930.815"/>
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="304.657" y1="884.981" x2="304.657" y2="930.815"/>
					</g>
				</g>
				<g>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M223.594,822.77
						c-9.927,0-17.973,9.674-17.973,21.61v10.7v21.611h35.948V855.08v-10.7C241.569,832.443,233.523,822.77,223.594,822.77z"/>
					<path fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" d="M236.177,856.815
						c0,1.349-0.828,2.437-1.85,2.437c-1.019,0-1.847-1.088-1.847-2.437c0-1.346,0.828-2.434,1.847-2.434
						C235.35,854.382,236.177,855.47,236.177,856.815z"/>
					
						<rect x="205.621" y="876.691" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="35.948" height="65.061"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="205.621" y1="881.723" x2="241.569" y2="881.723"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="205.621" y1="887.451" x2="241.569" y2="887.451"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="205.621" y1="893.221" x2="241.569" y2="893.221"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="205.621" y1="897.536" x2="241.569" y2="897.536"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="205.621" y1="901.851" x2="241.569" y2="901.851"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="205.621" y1="907.898" x2="241.569" y2="907.898"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="205.621" y1="912.55" x2="241.569" y2="912.55"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="205.621" y1="916.948" x2="241.569" y2="916.948"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="204.993" y1="924.193" x2="240.942" y2="924.193"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="205.621" y1="930.815" x2="241.569" y2="930.815"/>
				</g>
			</g>
			<g>
				<polygon fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" points="292.502,684.14 292.502,660.339 
					280.553,660.339 280.553,671.558 261.02,650.979 185.007,725.644 332.43,726.199 				"/>
				<polygon fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" points="212.864,711.636 304.571,711.986 
					260.147,665.188 				"/>
				
					<rect x="243.908" y="688.59" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="31.015" height="17.731"/>
			</g>
		</g>
		<g id="build4_7_">
			<g>
				<g>
					
						<rect x="56.841" y="714.201" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="72.571" height="49.609"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="72.255" y1="714.201" x2="72.255" y2="763.811"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="113.685" y1="714.201" x2="113.685" y2="763.811"/>
				</g>
				<g>
					
						<rect x="56.841" y="791.469" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.771" height="53.225"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="56.841" y1="818.085" x2="80.613" y2="818.085"/>
				</g>
				<g>
					
						<rect x="101.798" y="791.469" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.773" height="53.225"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="101.798" y1="818.085" x2="125.571" y2="818.085"/>
				</g>
				<g>
					
						<rect x="56.841" y="870.928" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.771" height="53.225"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="56.841" y1="897.536" x2="80.613" y2="897.536"/>
				</g>
				<g>
					
						<rect x="101.798" y="870.928" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.773" height="53.225"/>
					
						<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="101.798" y1="897.536" x2="125.571" y2="897.536"/>
				</g>
			</g>
			<g>
				<rect y="689.397" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="144.51" height="252.354"/>
			</g>
			<g>
				<polygon fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" points="105.404,639.608 105.404,611.086 
					93.358,611.086 93.358,624.533 73.666,599.871 -2.957,689.339 145.652,690.004 				"/>
				<polygon fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" points="25.127,672.556 117.569,672.974 
					72.791,616.899 				"/>
				
					<rect x="56.418" y="644.938" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="31.267" height="21.249"/>
			</g>
		</g>
		<g id="build6_7_">
			
				<rect x="510.112" y="893.303" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="7.217" height="96.229"/>
			
				<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="517.329" y1="893.303" x2="510.112" y2="886.244"/>
		</g>
		<g>
			<g>
				<g id="build11_6_">
					<g id="build5_7_">
						
							<rect x="498.956" y="534.439" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="113.595" height="281.135"/>
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="516.21" y1="575.085" x2="516.21" y2="815.574"/>
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="592.061" y1="575.085" x2="592.061" y2="815.574"/>
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="579.119" y1="511.457" x2="579.119" y2="534.439"/>
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="564.021" y1="518.626" x2="564.021" y2="534.439"/>
					</g>
					<g id="build7_7_">
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="612.551" y1="815.574" x2="732.256" y2="815.574"/>
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="732.256" y1="815.574" x2="732.256" y2="859.252"/>
						
							<rect x="516.21" y="828.516" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="22.933" height="21.214"/>
						
							<rect x="564.021" y="828.516" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="22.647" height="21.214"/>
						
							<rect x="599.609" y="828.516" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="22.646" height="21.214"/>
						
							<rect x="641.365" y="828.516" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="20.793" height="21.214"/>
						
							<rect x="679.414" y="828.516" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.726" height="21.214"/>
						
							<rect x="516.21" y="870.195" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="22.933" height="23.025"/>
						
							<rect x="564.021" y="870.928" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="22.647" height="22.293"/>
						
							<rect x="599.609" y="870.928" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="22.646" height="22.293"/>
						
							<rect x="539.144" y="909.221" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="92.818" height="32.531"/>
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="539.144" y1="916.948" x2="631.962" y2="916.948"/>
					</g>
					<g id="build10_6_">
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="612.551" y1="650.035" x2="842.705" y2="650.035"/>
						<g>
							
								<rect x="631.962" y="705.17" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="77.648" height="27.364"/>
							
								<rect x="631.962" y="758.417" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="77.648" height="24.804"/>
							
								<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="672.404" y1="705.17" x2="672.404" y2="732.534"/>
							
								<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="672.404" y1="758.402" x2="672.404" y2="783.221"/>
						</g>
					</g>
					<g id="build9_6_">
						
							<rect x="732.256" y="660.01" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="338.628" height="199.039"/>
						<g>
							
								<rect x="732.256" y="660.01" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="90.589" height="36.114"/>
							
								<rect x="822.845" y="660.01" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="36.667" height="36.114"/>
							
								<rect x="859.512" y="660.01" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="211.373" height="36.114"/>
							
								<rect x="732.256" y="696.053" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="214.609" height="42.771"/>
							
								<rect x="946.865" y="696.124" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="37.745" height="42.699"/>
							
								<rect x="984.61" y="696.124" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="86.274" height="42.699"/>
							
								<rect x="732.256" y="738.823" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="252.354" height="35.127"/>
							
								<rect x="984.61" y="738.823" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="43.138" height="35.127"/>
							
								<rect x="1027.748" y="738.823" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="43.137" height="35.127"/>
							
								<rect x="732.256" y="773.95" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="338.628" height="41.264"/>
							
								<rect x="859.512" y="773.95" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="42.059" height="41.264"/>
							
								<rect x="777.551" y="815.214" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="39.126" height="43.835"/>
							
								<rect x="959.807" y="815.214" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="40.98" height="43.835"/>
						</g>
					</g>
					<g id="buld8_6_">
						
							<rect x="842.705" y="354.008" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="186.121" height="306.002"/>
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="984.61" y1="343.223" x2="984.61" y2="354.008"/>
						
							<line fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" x1="1006.179" y1="319.497" x2="1006.179" y2="354.008"/>
						<g>
							
								<rect x="842.705" y="405.772" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="186.121" height="17.255"/>
							
								<rect x="842.705" y="463.255" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="185.043" height="19.051"/>
							
								<rect x="842.705" y="526.528" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="186.121" height="19.438"/>
							
								<rect x="842.705" y="582.615" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="186.121" height="17.256"/>
							
								<rect x="880.541" y="641.946" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="118.089" height="18.063"/>
							
								<rect x="880.541" y="405.772" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="49.069" height="17.255"/>
							
								<rect x="965.198" y="405.772" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="40.98" height="17.255"/>
							
								<rect x="864.904" y="463.255" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="44.216" height="19.051"/>
							
								<rect x="946.865" y="463.255" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="43.138" height="19.051"/>
							
								<rect x="880.541" y="526.528" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="43.676" height="19.438"/>
							
								<rect x="965.198" y="526.528" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="45.295" height="19.438"/>
							
								<rect x="864.904" y="582.615" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="44.216" height="17.256"/>
							
								<rect x="952.258" y="582.615" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="46.372" height="17.256"/>
						</g>
					</g>
					
						<rect x="1287.726" y="433.163" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="82.167" height="426.199"/>
					
						<rect x="1113.412" y="314.434" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="174.314" height="424.657"/>
					
						<rect x="1070.885" y="739.005" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="216.841" height="120.247"/>
					
						<rect x="1259.808" y="314.433" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="27.918" height="424.431"/>
					
						<rect x="1113.412" y="314.433" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="31.816" height="424.658"/>
					<g>
						
							<rect x="1219.858" y="346.897" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.548" height="23.523"/>
						
							<rect x="1157.872" y="346.898" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.547" height="23.524"/>
						
							<rect x="1218.997" y="394.349" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.548" height="23.523"/>
						
							<rect x="1157.01" y="394.354" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.546" height="23.523"/>
						
							<rect x="1220.479" y="432.974" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.547" height="23.523"/>
						
							<rect x="1158.493" y="432.979" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.547" height="23.523"/>
						
							<rect x="1219.618" y="480.43" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.547" height="23.523"/>
						
							<rect x="1157.632" y="480.435" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.547" height="23.523"/>
					</g>
					<g>
						
							<rect x="1219.115" y="521.401" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.546" height="23.523"/>
						
							<rect x="1157.128" y="521.406" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.547" height="23.523"/>
						
							<rect x="1218.253" y="568.857" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.546" height="23.523"/>
						
							<rect x="1156.266" y="568.861" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.547" height="23.523"/>
						
							<rect x="1219.736" y="607.482" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.547" height="23.523"/>
						
							<rect x="1157.749" y="607.487" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.548" height="23.522"/>
						
							<rect x="1218.875" y="654.938" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.547" height="23.522"/>
						
							<rect x="1156.888" y="654.942" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.548" height="23.523"/>
					</g>
					
						<rect x="1070.885" y="828.516" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="216.841" height="8.896"/>
					
						<rect x="1070.885" y="843.951" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="216.841" height="5.778"/>
					
						<rect x="1088.005" y="758.402" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="25.883" height="24.818"/>
					
						<rect x="1158.493" y="758.402" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.547" height="24.818"/>
					
						<rect x="1232.753" y="758.417" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.813" height="24.804"/>
					
						<rect x="1314.412" y="417.056" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="0" height="15.676"/>
				</g>
				<g>
					
						<rect x="612.551" y="575.085" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="230.154" height="74.95"/>
					
						<rect x="612.551" y="631.01" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="230.154" height="19.025"/>
					
						<rect x="649.397" y="550.794" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="193.308" height="24.291"/>
					
						<rect x="659.229" y="592.381" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="12.724" height="15.106"/>
					
						<rect x="704.853" y="589.437" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="12.724" height="15.106"/>
					
						<rect x="749.445" y="592.381" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="12.724" height="15.106"/>
					
						<rect x="795.07" y="589.437" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="12.723" height="15.106"/>
				</g>
			</g>
			<g>
				
					<rect x="1312.542" y="502.366" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="33.343" height="356.995"/>
				
					<rect x="1328.81" y="522.949" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="0" height="336.412"/>
				
					<rect x="1301.775" y="450.322" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="20.132" height="21.409"/>
				
					<rect x="1338.425" y="450.322" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="24.714" height="21.409"/>
			</g>
		</g>
		
			<rect x="1387.707" y="725.146" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="130.729" height="118.805"/>
		
			<rect x="1387.707" y="764.146" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="130.727" height="34.983"/>
		
			<rect x="1387.707" y="725.146" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="44.641" height="118.805"/>
		
			<rect x="1477.078" y="725.146" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="41.357" height="118.805"/>
		
			<rect x="56.841" y="714.201" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="72.571" height="49.609"/>
		
			<rect x="72.255" y="714.201" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="41.429" height="49.609"/>
		
			<rect x="56.841" y="791.469" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.771" height="53.225"/>
		
			<rect x="101.798" y="791.469" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.773" height="53.225"/>
		
			<rect x="56.841" y="818.085" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.771" height="0"/>
		
			<rect x="101.798" y="818.085" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.773" height="0"/>
		
			<rect x="56.841" y="870.928" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.771" height="53.225"/>
		
			<rect x="101.798" y="870.928" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.773" height="53.225"/>
		
			<rect x="101.798" y="897.536" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.773" height="0"/>
		
			<rect x="56.841" y="897.536" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="23.771" height="0"/>
		
			<rect x="1387.707" y="764.146" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="44.641" height="34.224"/>
		
			<rect x="1477.078" y="763.811" fill="none" stroke="#A7A9AC" stroke-width="0.5" stroke-miterlimit="10" width="41.355" height="35.318"/>
	</g>
</g>
<g id="cloud2">
	<path fill="#F7F8F9" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" d="M2686.251,304.397
		c8.752-19.412-18.463-24.464-18.463-24.464c-30.195-64.367-62.549-7.72-62.549-7.72c-10.783-10.617-19.41,0-19.41,0
		c0-80.714-70.723-48.699-70.723-48.699c-103.074-121.862-160.525,12.941-160.525,12.941c-51.15-28.039-75.027,28.039-75.027,28.039
		c-14.02-14.02-24.803,0-24.803,0c-30.199-35.588-52.688,7.718-52.688,7.718c-36.512-3.066-24.959,36.498-24.959,36.498h510.881
		L2686.251,304.397z"/>
</g>
<g id="build5_1_">
	
		<rect x="3024.738" y="662.383" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="89.904" height="244.227"/>
	
		<rect x="3282.498" y="743.319" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="71.176" height="163.553"/>
	
		<rect x="3114.642" y="264.495" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="167.855" height="642.377"/>
	
		<rect x="3114.642" y="264.495" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="167.855" height="15.438"/>
	
		<rect x="3282.498" y="637.161" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="35.588" height="106.158"/>
	<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="3197.761,906.609 3199.378,906.609 
		3293.201,906.609 3199.378,49.482 3197.761,49.482 3103.937,906.609 	"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="1930.369" y1="673.25" x2="1943.369" y2="673.25"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3178.535" y1="272.213" x2="3218.322" y2="272.213"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3175.171" y1="308.588" x2="3220.564" y2="308.588"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3170.689" y1="349.486" x2="3224.486" y2="349.486"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3164.525" y1="398.726" x2="3230.65" y2="398.726"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3238.496" y1="454.142" x2="3159.482" y2="454.142"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3245.22" y1="516.345" x2="3155.558" y2="516.345"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3250.824" y1="585.685" x2="3146.593" y2="585.685"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3257.548" y1="662.383" x2="3141.548" y2="662.383"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3269.318" y1="758.868" x2="3132.023" y2="758.868"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3278.843" y1="848.357" x2="3120.253" y2="848.357"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3299.542" y1="658.068" x2="3316.748" y2="658.068"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3299.996" y1="678.614" x2="3317.203" y2="678.614"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3299.886" y1="699.049" x2="3317.093" y2="699.049"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3376.037" y1="830.617" x2="3376.037" y2="891.94"/>
	
		<rect x="2590.753" y="338.702" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="2.662" height="0"/>
	
		<rect x="3129.074" y="299.004" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="19.699" height="19.412"/>
	
		<rect x="3129.074" y="326.457" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="19.699" height="19.411"/>
	
		<rect x="3129.074" y="355.083" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="19.699" height="19.413"/>
	
		<rect x="3129.074" y="382.533" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="19.699" height="19.412"/>
	
		<rect x="3248.921" y="294.399" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="19.699" height="19.411"/>
	
		<rect x="3248.921" y="321.848" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="19.699" height="19.413"/>
	
		<rect x="3248.921" y="350.477" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="19.699" height="19.412"/>
	
		<rect x="3248.921" y="377.926" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="19.699" height="19.412"/>
	<g>
		
			<rect x="3042.587" y="698.58" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="56.332" height="0"/>
		
			<rect x="3041.527" y="727.548" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="56.332" height="0"/>
		
			<rect x="3043.121" y="758.868" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="56.33" height="0"/>
		
			<rect x="3042.058" y="787.835" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="56.33" height="0"/>
		
			<rect x="3043.65" y="815.078" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="56.332" height="0"/>
		
			<rect x="3042.587" y="844.043" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="56.332" height="0"/>
		
			<rect x="3041.527" y="869.346" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="56.332" height="0"/>
	</g>
	
		<rect x="3301.828" y="756.506" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="11.086" height="57.989"/>
	
		<rect x="3329.65" y="756.506" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="11.084" height="57.989"/>
	
		<rect x="3302.945" y="833.949" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="11.086" height="57.991"/>
	
		<rect x="3330.765" y="833.949" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="11.086" height="57.991"/>
	
		<rect x="3329.65" y="724.122" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="12.201" height="19.197"/>
	<g>
		
			<rect x="3299.542" y="658.068" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="17.205" height="0"/>
		
			<rect x="3299.996" y="678.614" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="17.207" height="0"/>
		
			<rect x="3299.886" y="699.049" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="17.207" height="0"/>
	</g>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3041.527" y1="727.548" x2="3097.859" y2="727.548"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3043.121" y1="758.868" x2="3099.451" y2="758.868"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3042.058" y1="787.835" x2="3098.388" y2="787.835"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3043.65" y1="815.078" x2="3099.982" y2="815.078"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3042.587" y1="844.043" x2="3098.919" y2="844.043"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="3041.527" y1="869.346" x2="3097.859" y2="869.346"/>
	
		<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="2562.357" y1="944.987" x2="2669.947" y2="944.987"/>
</g>
<g id="build2_1_">
	<g id="build2">
		
			<rect x="350.851" y="503.544" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="148.104" height="415.425"/>
		
			<rect x="350.851" y="918.969" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="165.359" height="22.774"/>
		
			<rect x="374.577" y="503.544" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="45.294" height="415.425"/>
		
			<rect x="437.845" y="503.544" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="41.701" height="415.425"/>
		<g>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="518.617" x2="407.648" y2="518.617"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="534.431" x2="407.648" y2="534.431"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="551.687" x2="407.648" y2="551.687"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="566.784" x2="407.648" y2="566.784"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="582.606" x2="407.648" y2="582.606"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="599.862" x2="407.648" y2="599.862"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="614.951" x2="407.648" y2="614.951"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="630.773" x2="407.648" y2="630.773"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="648.029" x2="407.648" y2="648.029"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="663.127" x2="407.648" y2="663.127"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="678.943" x2="407.648" y2="678.943"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="696.196" x2="407.648" y2="696.196"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="710.218" x2="407.648" y2="710.218"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="726.039" x2="407.648" y2="726.039"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="743.293" x2="407.648" y2="743.293"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="758.394" x2="407.648" y2="758.394"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="774.207" x2="407.648" y2="774.207"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="385.361" y1="791.461" x2="407.648" y2="791.461"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="386.08" y1="806.197" x2="408.366" y2="806.197"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="386.08" y1="822.02" x2="408.366" y2="822.02"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="386.08" y1="839.275" x2="408.366" y2="839.275"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="386.08" y1="854.373" x2="408.366" y2="854.373"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="386.08" y1="870.187" x2="408.366" y2="870.187"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="386.08" y1="887.442" x2="408.366" y2="887.442"/>
		</g>
		<g>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="518.617" x2="466.602" y2="518.617"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="534.431" x2="466.602" y2="534.431"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="551.687" x2="466.602" y2="551.687"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="566.784" x2="466.602" y2="566.784"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="582.606" x2="466.602" y2="582.606"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="599.862" x2="466.602" y2="599.862"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="614.951" x2="466.602" y2="614.951"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="630.773" x2="466.602" y2="630.773"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="648.029" x2="466.602" y2="648.029"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="663.127" x2="466.602" y2="663.127"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="678.943" x2="466.602" y2="678.943"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="696.196" x2="466.602" y2="696.196"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="710.218" x2="466.602" y2="710.218"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="726.039" x2="466.602" y2="726.039"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="743.293" x2="466.602" y2="743.293"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="758.394" x2="466.602" y2="758.394"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="774.207" x2="466.602" y2="774.207"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="444.315" y1="791.461" x2="466.602" y2="791.461"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="445.033" y1="806.197" x2="467.32" y2="806.197"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="445.033" y1="822.02" x2="467.32" y2="822.02"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="445.033" y1="839.275" x2="467.32" y2="839.275"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="445.033" y1="854.373" x2="467.32" y2="854.373"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="445.033" y1="870.187" x2="467.32" y2="870.187"/>
			
				<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="445.033" y1="887.442" x2="467.32" y2="887.442"/>
		</g>
		<polygon fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" points="455.458,464.736 455.458,414.39 
			393.271,414.39 393.271,464.741 357.702,464.899 377.197,503.264 424.905,503.047 472.61,503.264 492.107,464.899 		"/>
		
			<rect x="444.315" y="432.723" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="1.901" height="16.715"/>
		<g>
			
				<rect x="385.361" y="472.624" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="22.288" height="19.951"/>
			
				<rect x="446.217" y="472.624" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="20.385" height="19.951"/>
			
				<rect x="405.236" y="432.723" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="2.412" height="16.715"/>
			
				<rect x="416.56" y="432.723" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="1.618" height="16.715"/>
			
				<rect x="428.962" y="432.723" fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" width="1.617" height="16.715"/>
		</g>
		
			<line fill="#FFFFFF" stroke="#BCBEC0" stroke-width="3" stroke-miterlimit="10" x1="393.811" y1="464.968" x2="455.997" y2="464.968"/>
	</g>
</g>
</svg>

`